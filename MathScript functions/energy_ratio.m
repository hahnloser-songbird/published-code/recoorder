function level = energy_ratio(transform)

level = sum(transform(9:16))/sum(transform(1:8));

end
