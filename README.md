Reference (please cite):
DOI: 10.3929/ethz-b-000617741
Website: https://gitlab.switch.ch/hahnloser-songbird/published-code/recoorder

This code is licensed via the MIT License (see included file LICENSE).
Copyright: (c) 2023 ETH Zurich, Joshua A. Herbst, Andreas Kotowicz, Claude Wang, Jörg Rychen, Alessandro Canopoli, Gagan Narula, Anja T. Zai, Richard H.R. Hahnloser; D-ITET, Institute of Neuroinformatics, Richard H.R. Hahnloser

Work address: Institute of Neuroinformatics, Winterthurerstrasse 8057,
ETH Zurich and University of Zurich

Authors: Joshua A. Herbst, Andreas Kotowicz, Claude Wang, Jörg Rychen, Alessandro Canopoli, Gagan Narula, Anja T. Zai, Richard H.R. Hahnloser

System requirements: LabVIEW 2019

How to install the recoorder executable (non-developer mode):

- Install LabVIEW Runtime: 
  https://www.ni.com/de-ch/support/downloads/software-products/download.labview.html#477380
  select: 2019, runtime, 32 bit

- Install necessary NI drivers
  ni845x.dll        -   http://www.ni.com/download/ni-845x-2.0/2647/en/ 
  niimaq.dll        -   http://www.ni.com/download/ni-vision-acquisition-software-january-2017/6606/en/
  nilvaiu.dll       -   http://www.ni.com/fr-fr/support/downloads/drivers/download.ni-daqmx.html#291872
  NI-488.2 17.6.0   -   http://www.ni.com/download/ni-488.2-17.6/7272/en/ 
  reboot

- run recoorder\build\recoorder.executable

How to install the recoorder for developer mode: 

- Install LavVIEW 2019
- Install necessary NI drivers listed above
- Install LabVIEW Statechart Module

Note: 
- Make sure automatic reboot is disabled
- The recoorder uses the regional settings when writing text files. Set 'decimal seperator' to '.'

