﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="19008000">
	<Property Name="CCSymbols" Type="Str"></Property>
	<Property Name="NI.Project.Description" Type="Str"></Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="activator" Type="Folder">
			<Property Name="NI.SortType" Type="Int">0</Property>
			<Item Name="activator_access" Type="Folder">
				<Item Name="activator_names_get.vi" Type="VI" URL="../classes/activator/public access/activator_names_get.vi"/>
				<Item Name="activator_notifications_get.vi" Type="VI" URL="../classes/activator/public access/activator_notifications_get.vi"/>
				<Item Name="activator_queues_fg.vi" Type="VI" URL="../classes/activator/activator_queues/activator_queues_fg.vi"/>
				<Item Name="activators_external_playback_get.vi" Type="VI" URL="../classes/activator/public access/activators_external_playback_get.vi"/>
			</Item>
			<Item Name="activator_subVIs" Type="Folder">
				<Item Name="activator_add.vi" Type="VI" URL="../classes/activator/activator_add.vi"/>
				<Item Name="activator_edit.vi" Type="VI" URL="../classes/activator/activator_edit.vi"/>
				<Item Name="activator_edit_running.vi" Type="VI" URL="../classes/activator/activator_edit_running.vi"/>
				<Item Name="activator_help.vi" Type="VI" URL="../classes/activator/activator_help.vi"/>
				<Item Name="activator_remove.vi" Type="VI" URL="../classes/activator/activator_remove.vi"/>
				<Item Name="activator_settings_txt.vi" Type="VI" URL="../classes/activator/helper/activator_settings_txt.vi"/>
				<Item Name="activators_clear.vi" Type="VI" URL="../classes/activator/activators_clear.vi"/>
				<Item Name="activators_run.vi" Type="VI" URL="../classes/activator/activators_run.vi"/>
				<Item Name="enqueue_activators.vi" Type="VI" URL="../classes/activator/activator_queues/enqueue_activators.vi"/>
				<Item Name="select_activator_class.vi" Type="VI" URL="../classes/activator/select_activator_class.vi"/>
			</Item>
			<Item Name="activator.lvclass" Type="LVClass" URL="../classes/activator/activator.lvclass"/>
			<Item Name="activator_airpuff.lvclass" Type="LVClass" URL="../classes/activator/activator_airpuff/activator_airpuff.lvclass"/>
			<Item Name="activator_deaftut.lvclass" Type="LVClass" URL="../classes/activator/activator_deaftut/activator_deaftut.lvclass"/>
			<Item Name="activator_detector_level_2_AO.lvclass" Type="LVClass" URL="../classes/activator/activator_detector_level_2_AO/activator_detector_level_2_AO.lvclass"/>
			<Item Name="activator_external_playback.lvclass" Type="LVClass" URL="../classes/activator/activator_external_playback/activator_external_playback.lvclass"/>
			<Item Name="activator_LMAN_HVC_stims.lvclass" Type="LVClass" URL="../classes/activator/activator_LMAN_HVC_stims/activator_LMAN_HVC_stims.lvclass"/>
			<Item Name="activator_pitch_shift.lvclass" Type="LVClass" URL="../classes/activator/activator_pitch_shift/activator_pitch_shift.lvclass"/>
			<Item Name="activator_poisson_non_move.lvclass" Type="LVClass" URL="../classes/activator/activator_poisson_non_move/activator_poisson_non_move.lvclass"/>
			<Item Name="activator_stim_playback.lvclass" Type="LVClass" URL="../classes/activator/activator_stim_playback/activator_stim_playback.lvclass"/>
			<Item Name="activator_string_2_wav.lvclass" Type="LVClass" URL="../classes/activator/activator_string_2_wav/activator_string_2_wav.lvclass"/>
			<Item Name="activator_syllable_gap_stretch.lvclass" Type="LVClass" URL="../classes/activator/activator_syllable_gap_stretch/activator_syllable_gap_stretch.lvclass"/>
			<Item Name="activator_syllable_stretch.lvclass" Type="LVClass" URL="../classes/activator/activator_syllable_stretch/activator_syllable_stretch.lvclass"/>
			<Item Name="activator_sylstretch_mathscript.lvclass" Type="LVClass" URL="../classes/activator/activator_sylstretch_mathscript/activator_sylstretch_mathscript.lvclass"/>
			<Item Name="activator_template_mathscript.lvclass" Type="LVClass" URL="../classes/activator/activator_template_mathscript/activator_template_mathscript.lvclass"/>
			<Item Name="activator_threshold.lvclass" Type="LVClass" URL="../classes/activator/activator_threshold/activator_threshold.lvclass"/>
			<Item Name="activator_threshold_history.lvclass" Type="LVClass" URL="../classes/activator/activator_threshold_history/activator_threshold_history.lvclass"/>
			<Item Name="activators.ctl" Type="VI" URL="../classes/activator/activators.ctl"/>
		</Item>
		<Item Name="controls" Type="Folder">
			<Item Name="1darray_fg_reference.ctl" Type="VI" URL="../controls/1darray_fg_reference.ctl"/>
			<Item Name="activator_tag.ctl" Type="VI" URL="../controls/activator_tag.ctl"/>
			<Item Name="channel_info.ctl" Type="VI" URL="../controls/channel_info.ctl"/>
			<Item Name="channel_name_filtering.ctl" Type="VI" URL="../controls/channel_name_filtering.ctl"/>
			<Item Name="channels_per_type.ctl" Type="VI" URL="../controls/channels_per_type.ctl"/>
			<Item Name="DAQ_input.ctl" Type="VI" URL="../controls/DAQ_input.ctl"/>
			<Item Name="DAQ_output.ctl" Type="VI" URL="../controls/DAQ_output.ctl"/>
			<Item Name="detectors_activators_effectors.ctl" Type="VI" URL="../controls/detectors_activators_effectors.ctl"/>
			<Item Name="file_type.ctl" Type="VI" URL="../controls/file_type.ctl"/>
			<Item Name="filter_name.ctl" Type="VI" URL="../controls/filter_name.ctl"/>
			<Item Name="general_settings.ctl" Type="VI" URL="../controls/general_settings.ctl"/>
			<Item Name="internal_detection_queue.ctl" Type="VI" URL="../controls/internal_detection_queue.ctl"/>
			<Item Name="investigators.txt" Type="Document" URL="../controls/investigators.txt"/>
			<Item Name="investigators_names_get.vi" Type="VI" URL="../controls/investigators_names_get.vi"/>
			<Item Name="plugin_notification.ctl" Type="VI" URL="../controls/plugin_notification.ctl"/>
			<Item Name="plugin_notification_array.ctl" Type="VI" URL="../controls/plugin_notification_array.ctl"/>
			<Item Name="plugin_notification_tag.ctl" Type="VI" URL="../controls/plugin_notification_tag.ctl"/>
			<Item Name="plugin_register_modes.ctl" Type="VI" URL="../controls/plugin_register_modes.ctl"/>
			<Item Name="recoorder states.ctl" Type="VI" URL="../controls/recoorder states.ctl"/>
			<Item Name="recorder_parameters.ctl" Type="VI" URL="../controls/recorder_parameters.ctl"/>
			<Item Name="save_case.ctl" Type="VI" URL="../controls/save_case.ctl"/>
			<Item Name="save_message_type.ctl" Type="VI" URL="../controls/save_message_type.ctl"/>
			<Item Name="save_parameters.ctl" Type="VI" URL="../controls/save_parameters.ctl"/>
			<Item Name="save_queue_cluster.ctl" Type="VI" URL="../controls/save_queue_cluster.ctl"/>
			<Item Name="settings.ctl" Type="VI" URL="../controls/settings.ctl"/>
			<Item Name="setup_observation.ctl" Type="VI" URL="../controls/setup_observation.ctl"/>
			<Item Name="setup_parameters.ctl" Type="VI" URL="../controls/setup_parameters.ctl"/>
			<Item Name="setups.txt" Type="Document" URL="../controls/setups.txt"/>
			<Item Name="setups_and_channels.ctl" Type="VI" URL="../controls/setups_and_channels.ctl"/>
			<Item Name="setups_names_get.vi" Type="VI" URL="../controls/setups_names_get.vi"/>
			<Item Name="statemachine.ctl" Type="VI" URL="../controls/statemachine.ctl"/>
			<Item Name="transform_info.ctl" Type="VI" URL="../controls/transform_info.ctl"/>
			<Item Name="UDP_message_in.ctl" Type="VI" URL="../controls/UDP_message_in.ctl"/>
			<Item Name="UDP_message_out.ctl" Type="VI" URL="../controls/UDP_message_out.ctl"/>
			<Item Name="a-e transport element.ctl" Type="VI" URL="../controls/a-e transport element.ctl"/>
			<Item Name="TMDS_property_names+values.ctl" Type="VI" URL="../controls/TMDS_property_names+values.ctl"/>
		</Item>
		<Item Name="detector" Type="Folder">
			<Property Name="NI.SortType" Type="Int">0</Property>
			<Item Name="detector_access" Type="Folder">
				<Item Name="detector_queues_fg.vi" Type="VI" URL="../classes/detector/detector_queues/detector_queues_fg.vi"/>
				<Item Name="detectors_dummy_get.vi" Type="VI" URL="../classes/detector/public access/detectors_dummy_get.vi"/>
			</Item>
			<Item Name="detector_subVIs" Type="Folder">
				<Item Name="detector_add.vi" Type="VI" URL="../classes/detector/detector_add.vi"/>
				<Item Name="detector_edit.vi" Type="VI" URL="../classes/detector/detector_edit.vi"/>
				<Item Name="detector_edit_running.vi" Type="VI" URL="../classes/detector/detector_edit_running.vi"/>
				<Item Name="detector_help.vi" Type="VI" URL="../classes/detector/detector_help.vi"/>
				<Item Name="detector_remove.vi" Type="VI" URL="../classes/detector/detector_remove.vi"/>
				<Item Name="detector_settings_txt.vi" Type="VI" URL="../classes/detector/detector_settings_txt.vi"/>
				<Item Name="detectors_clear.vi" Type="VI" URL="../classes/detector/detectors_clear.vi"/>
				<Item Name="detectors_run.vi" Type="VI" URL="../classes/detector/detectors_run.vi"/>
				<Item Name="enqueue_detectors.vi" Type="VI" URL="../classes/detector/detector_queues/enqueue_detectors.vi"/>
				<Item Name="select_detector_class.vi" Type="VI" URL="../classes/detector/select_detector_class.vi"/>
			</Item>
			<Item Name="detector.lvclass" Type="LVClass" URL="../classes/detector/detector.lvclass"/>
			<Item Name="detector_analog.lvclass" Type="LVClass" URL="../classes/detector/detector_analog/detector_analog.lvclass"/>
			<Item Name="detector_digital.lvclass" Type="LVClass" URL="../classes/detector/detector_digital/detector_digital.lvclass"/>
			<Item Name="detector_digital_pulse.lvclass" Type="LVClass" URL="../classes/detector/detector_digital_pulse/detector_digital_pulse.lvclass"/>
			<Item Name="detector_dummy.lvclass" Type="LVClass" URL="../classes/detector/detector_dummy/detector_dummy.lvclass"/>
			<Item Name="detector_filter.lvclass" Type="LVClass" URL="../classes/detector/detector_filter/detector_filter.lvclass"/>
			<Item Name="detector_freq_band_energy.lvclass" Type="LVClass" URL="../classes/detector/detector_freq_band_energy/detector_freq_band_energy.lvclass"/>
			<Item Name="detector_harmonics.lvclass" Type="LVClass" URL="../classes/detector/detector_harmonics/detector_harmonics.lvclass"/>
			<Item Name="detector_harmonics2.lvclass" Type="LVClass" URL="../classes/detector/detector_harmonics2/detector_harmonics2.lvclass"/>
			<Item Name="detector_max.lvclass" Type="LVClass" URL="../classes/detector/detector_max/detector_max.lvclass"/>
			<Item Name="detector_max_ac_power.lvclass" Type="LVClass" URL="../classes/detector/detector_max_ac_power/detector_max_ac_power.lvclass"/>
			<Item Name="detector_motion_UDP.lvclass" Type="LVClass" URL="../classes/detector/detector_motion_UDP/detector_motion_UDP.lvclass"/>
			<Item Name="detector_perceptron_analyze.lvclass" Type="LVClass" URL="../classes/detector/detector_perceptron_analyze/detector_perceptron_analyze.lvclass"/>
			<Item Name="detector_perceptron_flatclust.lvclass" Type="LVClass" URL="../classes/detector/detector_perceptron_flatclust/detector_perceptron_flatclust.lvclass"/>
			<Item Name="detector_pitch.lvclass" Type="LVClass" URL="../classes/detector/detector_pitch/detector_pitch.lvclass"/>
			<Item Name="detector_principal_component.lvclass" Type="LVClass" URL="../classes/detector/detector_principal_component/detector_principal_component.lvclass"/>
			<Item Name="detector_rms.lvclass" Type="LVClass" URL="../classes/detector/detector_rms/detector_rms.lvclass"/>
			<Item Name="detector_spectrogram.lvclass" Type="LVClass" URL="../classes/detector/detector_spectrogram/detector_spectrogram.lvclass"/>
			<Item Name="detector_spl.lvclass" Type="LVClass" URL="../classes/detector/detector_spl/detector_spl.lvclass"/>
			<Item Name="detectors.ctl" Type="VI" URL="../classes/detector/detectors.ctl"/>
		</Item>
		<Item Name="effector" Type="Folder">
			<Item Name="effector_access" Type="Folder">
				<Item Name="effector_blocks_get.vi" Type="VI" URL="../classes/effector/public access/effector_blocks_get.vi"/>
				<Item Name="effector_names_get.vi" Type="VI" URL="../classes/effector/public access/effector_names_get.vi"/>
				<Item Name="effector_queues_fg.vi" Type="VI" URL="../classes/effector/effector_queues/effector_queues_fg.vi"/>
				<Item Name="effector_set_block.vi" Type="VI" URL="../classes/effector/public access/effector_set_block.vi"/>
				<Item Name="effector_set_trigger.vi" Type="VI" URL="../classes/effector/public access/effector_set_trigger.vi"/>
			</Item>
			<Item Name="effector_subVIs" Type="Folder">
				<Item Name="effector_add.vi" Type="VI" URL="../classes/effector/effector_add.vi"/>
				<Item Name="effector_edit.vi" Type="VI" URL="../classes/effector/effector_edit.vi"/>
				<Item Name="effector_edit_running.vi" Type="VI" URL="../classes/effector/effector_edit_running.vi"/>
				<Item Name="effector_help.vi" Type="VI" URL="../classes/effector/effector_help.vi"/>
				<Item Name="effector_remove.vi" Type="VI" URL="../classes/effector/effector_remove.vi"/>
				<Item Name="effector_settings_txt.vi" Type="VI" URL="../classes/effector/effector_settings_txt.vi"/>
				<Item Name="effectors_clear.vi" Type="VI" URL="../classes/effector/effectors_clear.vi"/>
				<Item Name="effectors_run.vi" Type="VI" URL="../classes/effector/effectors_run.vi"/>
				<Item Name="enqueue_effectors.vi" Type="VI" URL="../classes/effector/effector_queues/enqueue_effectors.vi"/>
				<Item Name="select_effector_class.vi" Type="VI" URL="../classes/effector/select_effector_class.vi"/>
			</Item>
			<Item Name="effector.lvclass" Type="LVClass" URL="../classes/effector/effector/effector.lvclass"/>
			<Item Name="effector_digital.lvclass" Type="LVClass" URL="../classes/effector/effector_digital/effector_digital.lvclass"/>
			<Item Name="effector_digital_pulse.lvclass" Type="LVClass" URL="../classes/effector/effector_digital_pulse/effector_digital_pulse.lvclass"/>
			<Item Name="effector_overwrite_output.lvclass" Type="LVClass" URL="../classes/effector/effector_overwrite_output/effector_overwrite_output.lvclass"/>
			<Item Name="effector_stim.lvclass" Type="LVClass" URL="../classes/effector/effector_stim/effector_stim.lvclass"/>
			<Item Name="effector_wav_playback.lvclass" Type="LVClass" URL="../classes/effector/effector_wav_playback/effector_wav_playback.lvclass"/>
			<Item Name="effectors.ctl" Type="VI" URL="../classes/effector/effectors.ctl"/>
			<Item Name="effector_analog.lvclass" Type="LVClass" URL="../classes/effector/effector_analog/effector_analog.lvclass"/>
			<Item Name="effector_5V_pulse.lvclass" Type="LVClass" URL="../classes/effector/effector_5V_pulse/effector_5V_pulse.lvclass"/>
			<Item Name="effector_5V_switch.lvclass" Type="LVClass" URL="../classes/effector/effector_5V_switch/effector_5V_switch.lvclass"/>
		</Item>
		<Item Name="fgs" Type="Folder">
			<Item Name="1darray" Type="Folder">
				<Item Name="1d_array_fg.vit" Type="VI" URL="../fgs/1d_array_fg/1d_array_fg.vit"/>
				<Item Name="1d_array_fg_reference.vi" Type="VI" URL="../fgs/1d_array_fg/1d_array_fg_reference.vi"/>
				<Item Name="1d_array_fg_set.vi" Type="VI" URL="../fgs/1d_array_fg/1d_array_fg_set.vi"/>
				<Item Name="1d_array_fg_get.vi" Type="VI" URL="../fgs/1d_array_fg/1d_array_fg_get.vi"/>
				<Item Name="1d_array_fg_release.vi" Type="VI" URL="../fgs/1d_array_fg/1d_array_fg_release.vi"/>
			</Item>
			<Item Name="boolean" Type="Folder">
				<Item Name="boolean_fg.vit" Type="VI" URL="../fgs/boolean_fg/boolean_fg.vit"/>
				<Item Name="boolean_fg_reference.vi" Type="VI" URL="../fgs/boolean_fg/boolean_fg_reference.vi"/>
				<Item Name="boolean_fg_set.vi" Type="VI" URL="../fgs/boolean_fg/boolean_fg_set.vi"/>
				<Item Name="boolean_fg_get.vi" Type="VI" URL="../fgs/boolean_fg/boolean_fg_get.vi"/>
			</Item>
			<Item Name="double fg" Type="Folder">
				<Item Name="scalar_fg.vit" Type="VI" URL="../fgs/scalar_fg/scalar_fg.vit"/>
				<Item Name="scalar_fg_reference.vi" Type="VI" URL="../fgs/scalar_fg/scalar_fg_reference.vi"/>
				<Item Name="scalar_fg_set.vi" Type="VI" URL="../fgs/scalar_fg/scalar_fg_set.vi"/>
				<Item Name="scalar_fg_get.vi" Type="VI" URL="../fgs/scalar_fg/scalar_fg_get.vi"/>
			</Item>
			<Item Name="save multi parameters" Type="Folder">
				<Item Name="save_multisetup_parameters.vi" Type="VI" URL="../fgs/save_multisetup_parameters/save_multisetup_parameters.vi"/>
				<Item Name="save_multisetup_parameters_get.vi" Type="VI" URL="../fgs/save_multisetup_parameters/save_multisetup_parameters_get.vi"/>
				<Item Name="save_multisetup_parameters_init.vi" Type="VI" URL="../fgs/save_multisetup_parameters/save_multisetup_parameters_init.vi"/>
				<Item Name="save_multisetup_parameters_set.vi" Type="VI" URL="../fgs/save_multisetup_parameters/save_multisetup_parameters_set.vi"/>
			</Item>
			<Item Name="string fg" Type="Folder">
				<Item Name="string_fg.vit" Type="VI" URL="../fgs/string_fg/string_fg.vit"/>
				<Item Name="string_fg_reference.vi" Type="VI" URL="../fgs/string_fg/string_fg_reference.vi"/>
				<Item Name="string_fg_set.vi" Type="VI" URL="../fgs/string_fg/string_fg_set.vi"/>
				<Item Name="string_fg_get.vi" Type="VI" URL="../fgs/string_fg/string_fg_get.vi"/>
			</Item>
			<Item Name="channels.vi" Type="VI" URL="../fgs/channels.vi"/>
			<Item Name="dae.vi" Type="VI" URL="../fgs/dae.vi"/>
			<Item Name="dae_subpanel.vi" Type="VI" URL="../fgs/dae_subpanel.vi"/>
			<Item Name="setups.vi" Type="VI" URL="../fgs/setups.vi"/>
			<Item Name="stop.vi" Type="VI" URL="../fgs/stop.vi"/>
		</Item>
		<Item Name="MathScript" Type="Folder">
			<Item Name="energy_ratio.m" Type="Document" URL="../MathScript functions/energy_ratio.m"/>
		</Item>
		<Item Name="plugins" Type="Folder">
			<Item Name="acute" Type="Folder">
				<Item Name="Gen Cont or Single Current Pulse.vi" Type="VI" URL="../plugins/Acute/Gen Cont or Single Current Pulse.vi"/>
			</Item>
			<Item Name="air puffs" Type="Folder">
				<Item Name="add_point.vi" Type="VI" URL="../plugins_subvi/air puffs/add_point.vi"/>
				<Item Name="air puffs multi setup multiWAV.vit" Type="VI" URL="../plugins/beta quality/air puffs multi setup multiWAV.vit"/>
				<Item Name="air puffs multi setup.vit" Type="VI" URL="../plugins/beta quality/air puffs multi setup.vit"/>
				<Item Name="air puffs new.vit" Type="VI" URL="../plugins/beta quality/air puffs new.vit"/>
				<Item Name="air puffs.vit" Type="VI" URL="../plugins/beta quality/air puffs.vit"/>
				<Item Name="air_puffs_enum.ctl" Type="VI" URL="../plugins_subvi/air puffs/air_puffs_enum.ctl"/>
				<Item Name="ap_open_logfile_gagan.vi" Type="VI" URL="../plugins_subvi/air puffs/ap_open_logfile_gagan.vi"/>
				<Item Name="calc_break.vi" Type="VI" URL="../plugins_subvi/air puffs/calc_break.vi"/>
				<Item Name="calc_perch_time.vi" Type="VI" URL="../plugins_subvi/air puffs/calc_perch_time.vi"/>
				<Item Name="check_perch.vi" Type="VI" URL="../plugins_subvi/air puffs/check_perch.vi"/>
				<Item Name="countdown.vi" Type="VI" URL="../plugins_subvi/air puffs/countdown.vi"/>
				<Item Name="get_effector_stim.vi" Type="VI" URL="../plugins_subvi/air puffs/get_effector_stim.vi"/>
				<Item Name="perch_fg.vi" Type="VI" URL="../plugins_subvi/air puffs/perch_fg.vi"/>
				<Item Name="perch_todo.vi" Type="VI" URL="../plugins_subvi/air puffs/perch_todo.vi"/>
				<Item Name="setups_activators.vi" Type="VI" URL="../plugins/beta quality/setups_activators.vi"/>
				<Item Name="song_file_selector_gagan.vi" Type="VI" URL="../plugins_subvi/air puffs/song_file_selector_gagan.vi"/>
				<Item Name="song_selector_josh.vi" Type="VI" URL="../plugins/beta quality/song_selector_josh.vi"/>
				<Item Name="song_selector_gagan.vi" Type="VI" URL="../plugins/beta quality/song_selector_gagan.vi"/>
				<Item Name="ap_open_logfile.vi" Type="VI" URL="../plugins_subvi/air puffs/ap_open_logfile.vi"/>
				<Item Name="ap_close_logfile.vi" Type="VI" URL="../plugins_subvi/air puffs/ap_close_logfile.vi"/>
				<Item Name="stop_air_puff.vi" Type="VI" URL="../plugins_subvi/air puffs/stop_air_puff.vi"/>
				<Item Name="stop_air_puff_queue.vi" Type="VI" URL="../plugins_subvi/air puffs/stop_air_puff_queue.vi"/>
				<Item Name="airpuff_simpleload.vi" Type="VI" URL="../subvi/helpers/airpuff_simpleload.vi"/>
				<Item Name="airpuff_permuteorder.vi" Type="VI" URL="../subvi/helpers/airpuff_permuteorder.vi"/>
			</Item>
			<Item Name="beta quality" Type="Folder">
				<Item Name="webcam" Type="Folder">
					<Item Name="acquisition_loop.vi" Type="VI" URL="../plugins_subvi/webcam/acquisition_loop.vi"/>
					<Item Name="check_encoders.vi" Type="VI" URL="../plugins_subvi/webcam/check_encoders.vi"/>
					<Item Name="compare_frames.vi" Type="VI" URL="../plugins_subvi/webcam/compare_frames.vi"/>
					<Item Name="do_motion.vi" Type="VI" URL="../plugins_subvi/webcam/do_motion.vi"/>
					<Item Name="get_avi_filename.vi" Type="VI" URL="../plugins_subvi/webcam/get_avi_filename.vi"/>
					<Item Name="get_video_mode.vi" Type="VI" URL="../plugins_subvi/webcam/get_video_mode.vi"/>
					<Item Name="get_webcam_queues.vi" Type="VI" URL="../plugins_subvi/webcam/get_webcam_queues.vi"/>
					<Item Name="image_motion_detection.vi" Type="VI" URL="../plugins_subvi/webcam/image_motion_detection.vi"/>
					<Item Name="image_motion_detectionBlocks.vi" Type="VI" URL="../plugins_subvi/webcam/image_motion_detectionBlocks.vi"/>
					<Item Name="image_motion_detectionMAX.vi" Type="VI" URL="../plugins_subvi/webcam/image_motion_detectionMAX.vi"/>
					<Item Name="init_webcam_queues.vi" Type="VI" URL="../plugins_subvi/webcam/init_webcam_queues.vi"/>
					<Item Name="motion.vi" Type="VI" URL="../plugins_subvi/remote_webcam/motion.vi"/>
					<Item Name="record_avi.vi" Type="VI" URL="../plugins_subvi/webcam/record_avi.vi"/>
					<Item Name="recorder_internal_notification_conversion.vi" Type="VI" URL="../plugins_subvi/webcam/recorder_internal_notification_conversion.vi"/>
					<Item Name="release_webcam_queues.vi" Type="VI" URL="../plugins_subvi/webcam/release_webcam_queues.vi"/>
					<Item Name="save_avi_video.vi" Type="VI" URL="../plugins_subvi/webcam/save_avi_video.vi"/>
					<Item Name="show_video_fg.vi" Type="VI" URL="../plugins_subvi/webcam/show_video_fg.vi"/>
					<Item Name="stop_webcam.vi" Type="VI" URL="../plugins_subvi/webcam/stop_webcam.vi"/>
					<Item Name="webcam.vi" Type="VI" URL="../plugins/beta quality/webcam.vi"/>
				</Item>
				<Item Name="webcam viewer" Type="Folder">
					<Item Name="webcam viewer.vi" Type="VI" URL="../plugins/beta quality/webcam viewer.vi"/>
				</Item>
			</Item>
			<Item Name="collision test" Type="Folder">
				<Item Name="check_AO_available.vi" Type="VI" URL="../plugins_subvi/collision test/check_AO_available.vi"/>
				<Item Name="AO_pulse_get.vi" Type="VI" URL="../plugins_subvi/collision test/AO_pulse_get.vi"/>
			</Item>
			<Item Name="intracellular" Type="Folder">
				<Item Name="intracellular song playback.vi" Type="VI" URL="../plugins/Acute/intracellular song playback.vi"/>
				<Item Name="RandomCurrentAmplitude.vi" Type="VI" URL="../plugins_subvi/intracellular_playback/RandomCurrentAmplitude.vi"/>
				<Item Name="CurrentAndPause.vi" Type="VI" URL="../plugins_subvi/intracellular_playback/CurrentAndPause.vi"/>
				<Item Name="RandomizePlaybackSelection.vi" Type="VI" URL="../plugins_subvi/intracellular_playback/RandomizePlaybackSelection.vi"/>
				<Item Name="randperm_generation.vi" Type="VI" URL="../plugins/Acute/randperm_generation.vi"/>
			</Item>
			<Item Name="labnotes" Type="Folder">
				<Item Name="add_time_comment.vi" Type="VI" URL="../plugins_subvi/labnotes/add_time_comment.vi"/>
				<Item Name="extract_filenumber.vi" Type="VI" URL="../plugins_subvi/labnotes/extract_filenumber.vi"/>
				<Item Name="file_string_comment.ctl" Type="VI" URL="../plugins_subvi/labnotes/file_string_comment.ctl"/>
				<Item Name="get@file.vi" Type="VI" URL="../plugins_subvi/labnotes/get@file.vi"/>
				<Item Name="get@time.vi" Type="VI" URL="../plugins_subvi/labnotes/get@time.vi"/>
				<Item Name="get_file_strings.vi" Type="VI" URL="../plugins_subvi/labnotes/get_file_strings.vi"/>
				<Item Name="get_time_strings.vi" Type="VI" URL="../plugins_subvi/labnotes/get_time_strings.vi"/>
				<Item Name="int2time.vi" Type="VI" URL="../plugins_subvi/labnotes/int2time.vi"/>
				<Item Name="labnotes-tags.txt" Type="Document" URL="../plugins_subvi/labnotes/labnotes-tags.txt"/>
				<Item Name="labnotes.vi" Type="VI" URL="../plugins/labnotes.vi"/>
				<Item Name="open_create_labnotess_txt.vi" Type="VI" URL="../plugins_subvi/labnotes/open_create_labnotess_txt.vi"/>
				<Item Name="read_categories_txt.vi" Type="VI" URL="../plugins_subvi/labnotes/read_categories_txt.vi"/>
				<Item Name="save_to_file.vi" Type="VI" URL="../plugins_subvi/labnotes/save_to_file.vi"/>
				<Item Name="tag_categories.vi" Type="VI" URL="../plugins_subvi/labnotes/tag_categories.vi"/>
				<Item Name="tag_sub_categories.vi" Type="VI" URL="../plugins_subvi/labnotes/tag_sub_categories.vi"/>
				<Item Name="time.ctl" Type="VI" URL="../plugins_subvi/labnotes/time.ctl"/>
				<Item Name="tagtype.ctl" Type="VI" URL="../plugins_subvi/labnotes/tagtype.ctl"/>
				<Item Name="tagtype2strings.vi" Type="VI" URL="../plugins_subvi/labnotes/tagtype2strings.vi"/>
				<Item Name="tagtype2tagstring.vi" Type="VI" URL="../plugins_subvi/labnotes/tagtype2tagstring.vi"/>
				<Item Name="time_string_comment.ctl" Type="VI" URL="../plugins_subvi/labnotes/time_string_comment.ctl"/>
				<Item Name="labnotes_check_day.vi" Type="VI" URL="../plugins_subvi/labnotes/labnotes_check_day.vi"/>
			</Item>
			<Item Name="mdcontrol" Type="Folder">
				<Item Name="actual_position.vi" Type="VI" URL="../plugins_subvi/microdrive_controller/actual_position.vi"/>
				<Item Name="DV-angle.vi" Type="VI" URL="../plugins_subvi/microdrive_controller/subvi/DV-angle.vi"/>
				<Item Name="get_position.vi" Type="VI" URL="../plugins_subvi/microdrive_controller/subvi/get_position.vi"/>
				<Item Name="mdcontrol_init.vi" Type="VI" URL="../plugins_subvi/microdrive_controller/subvi/mdcontrol_init.vi"/>
				<Item Name="microdrive controller.vi" Type="VI" URL="../plugins/microdrive controller.vi"/>
				<Item Name="microdrive controller new.vi" Type="VI" URL="../plugins/microdrive controller new.vi"/>
				<Item Name="read_controller.vi" Type="VI" URL="../plugins_subvi/microdrive_controller/subvi/read_controller.vi"/>
				<Item Name="read_position.vi" Type="VI" URL="../plugins_subvi/microdrive_controller/subvi/read_position.vi"/>
				<Item Name="search_microdrive_controller.vi" Type="VI" URL="../plugins_subvi/microdrive_controller/subvi/search_microdrive_controller.vi"/>
				<Item Name="SendCommandsToMicrodrive.vi" Type="VI" URL="../plugins_subvi/microdrive_controller/subvi/SendCommandsToMicrodrive.vi"/>
				<Item Name="SendComToMD.vi" Type="VI" URL="../plugins_subvi/microdrive_controller/subvi/SendComToMD.vi"/>
				<Item Name="set_current_position.vi" Type="VI" URL="../plugins_subvi/microdrive_controller/subvi/set_current_position.vi"/>
				<Item Name="set_output_voltage.vi" Type="VI" URL="../plugins_subvi/microdrive_controller/subvi/set_output_voltage.vi"/>
				<Item Name="startup_md.vi" Type="VI" URL="../plugins_subvi/microdrive_controller/subvi/startup_md.vi"/>
				<Item Name="stopmd.vi" Type="VI" URL="../plugins_subvi/microdrive_controller/subvi/stopmd.vi"/>
				<Item Name="write_logfile_md.vi" Type="VI" URL="../plugins_subvi/microdrive_controller/subvi/write_logfile_md.vi"/>
				<Item Name="get_move_command.vi" Type="VI" URL="../plugins_subvi/microdrive_controller/subvi/get_move_command.vi"/>
			</Item>
			<Item Name="mdcontrol2" Type="Folder">
				<Item Name="calibration_data.vi" Type="VI" URL="../plugins_subvi/microdrive controller 2/calibration_data.vi"/>
				<Item Name="check_keyboard.vi" Type="VI" URL="../plugins_subvi/microdrive controller 2/check_keyboard.vi"/>
				<Item Name="enum.ctl" Type="VI" URL="../plugins_subvi/microdrive controller 2/enum.ctl"/>
				<Item Name="go_to_target.vi" Type="VI" URL="../plugins_subvi/microdrive controller 2/go_to_target.vi"/>
				<Item Name="keypress_event.vi" Type="VI" URL="../plugins_subvi/microdrive controller 2/keypress_event.vi"/>
				<Item Name="log_position.vi" Type="VI" URL="../plugins_subvi/microdrive controller 2/log_position.vi"/>
				<Item Name="md_help.vi" Type="VI" URL="../plugins_subvi/microdrive controller 2/md_help.vi"/>
				<Item Name="md_stop_fg.vi" Type="VI" URL="../plugins_subvi/microdrive controller 2/md_stop_fg.vi"/>
				<Item Name="microdrive_controller_2.vi" Type="VI" URL="../plugins/beta quality/microdrive_controller_2.vi"/>
				<Item Name="move_message.vi" Type="VI" URL="../plugins_subvi/microdrive controller 2/move_message.vi"/>
				<Item Name="move_or_not.vi" Type="VI" URL="../plugins_subvi/microdrive controller 2/move_or_not.vi"/>
				<Item Name="new_target.vi" Type="VI" URL="../plugins_subvi/microdrive controller 2/new_target.vi"/>
				<Item Name="position_log.vi" Type="VI" URL="../plugins_subvi/microdrive controller 2/position_log.vi"/>
				<Item Name="read_text_file.vi" Type="VI" URL="../plugins_subvi/microdrive controller 2/read_text_file.vi"/>
				<Item Name="sensor_read.vi" Type="VI" URL="../plugins_subvi/microdrive controller 2/sensor_read.vi"/>
				<Item Name="write_to_text_file_full.vi" Type="VI" URL="../plugins_subvi/microdrive controller 2/write_to_text_file_full.vi"/>
				<Item Name="in_holding_range.vi" Type="VI" URL="../plugins_subvi/microdrive controller 2/in_holding_range.vi"/>
				<Item Name="step_to_target.vi" Type="VI" URL="../plugins_subvi/microdrive controller 2/step_to_target.vi"/>
				<Item Name="speed_update.vi" Type="VI" URL="../plugins_subvi/microdrive controller 2/speed_update.vi"/>
				<Item Name="write_to_text_file_zero.vi" Type="VI" URL="../plugins_subvi/microdrive controller 2/write_to_text_file_zero.vi"/>
				<Item Name="write_to_text_file_polymorphic.vi" Type="VI" URL="../plugins_subvi/microdrive controller 2/write_to_text_file_polymorphic.vi"/>
			</Item>
			<Item Name="oscilloscope" Type="Folder">
				<Item Name="channel_parameters.ctl" Type="VI" URL="../plugins_subvi/oscilloscope/channel_parameters.ctl"/>
				<Item Name="osci_buffer_to_plot.vi" Type="VI" URL="../plugins_subvi/oscilloscope/osci_buffer_to_plot.vi"/>
				<Item Name="osci_calc_powerspec.vi" Type="VI" URL="../plugins_subvi/oscilloscope/osci_calc_powerspec.vi"/>
				<Item Name="osci_collect_data_from_queue.vi" Type="VI" URL="../plugins_subvi/oscilloscope/osci_collect_data_from_queue.vi"/>
				<Item Name="osci_data_update.vi" Type="VI" URL="../plugins_subvi/oscilloscope/osci_data_update.vi"/>
				<Item Name="osci_fg_channel_color_get.vi" Type="VI" URL="../plugins_subvi/oscilloscope/osci_fg_channel_color_get.vi"/>
				<Item Name="osci_fg_channel_color_set.vi" Type="VI" URL="../plugins_subvi/oscilloscope/osci_fg_channel_color_set.vi"/>
				<Item Name="osci_fg_channel_offset_get.vi" Type="VI" URL="../plugins_subvi/oscilloscope/osci_fg_channel_offset_get.vi"/>
				<Item Name="osci_fg_channel_offset_set.vi" Type="VI" URL="../plugins_subvi/oscilloscope/osci_fg_channel_offset_set.vi"/>
				<Item Name="osci_fg_channel_parameters_get.vi" Type="VI" URL="../plugins_subvi/oscilloscope/osci_fg_channel_parameters_get.vi"/>
				<Item Name="osci_fg_channel_trigger_get.vi" Type="VI" URL="../plugins_subvi/oscilloscope/osci_fg_channel_trigger_get.vi"/>
				<Item Name="osci_fg_channel_trigger_set.vi" Type="VI" URL="../plugins_subvi/oscilloscope/osci_fg_channel_trigger_set.vi"/>
				<Item Name="osci_fg_get.vi" Type="VI" URL="../plugins_subvi/oscilloscope/osci_fg_get.vi"/>
				<Item Name="osci_fg_get_copy.vi" Type="VI" URL="../plugins_subvi/oscilloscope/osci_fg_get_copy.vi"/>
				<Item Name="osci_fg_init.vi" Type="VI" URL="../plugins_subvi/oscilloscope/osci_fg_init.vi"/>
				<Item Name="osci_fg_input_channels_set.vi" Type="VI" URL="../plugins_subvi/oscilloscope/osci_fg_input_channels_set.vi"/>
				<Item Name="osci_fg_rising-falling_set.vi" Type="VI" URL="../plugins_subvi/oscilloscope/osci_fg_rising-falling_set.vi"/>
				<Item Name="osci_fg_sec-div_set.vi" Type="VI" URL="../plugins_subvi/oscilloscope/osci_fg_sec-div_set.vi"/>
				<Item Name="osci_fg_set.vi" Type="VI" URL="../plugins_subvi/oscilloscope/osci_fg_set.vi"/>
				<Item Name="osci_fg_volts-div_set.vi" Type="VI" URL="../plugins_subvi/oscilloscope/osci_fg_volts-div_set.vi"/>
				<Item Name="osci_filter.vi" Type="VI" URL="../plugins_subvi/oscilloscope/osci_filter.vi"/>
				<Item Name="osci_parameters.ctl" Type="VI" URL="../plugins_subvi/oscilloscope/osci_parameters.ctl"/>
				<Item Name="osci_update_offset_cursor.vi" Type="VI" URL="../plugins_subvi/oscilloscope/osci_update_offset_cursor.vi"/>
				<Item Name="osci_update_trigger_cursor.vi" Type="VI" URL="../plugins_subvi/oscilloscope/osci_update_trigger_cursor.vi"/>
				<Item Name="oscilloscope.vi" Type="VI" URL="../plugins/oscilloscope.vi"/>
				<Item Name="osci_autoset.vi" Type="VI" URL="../plugins_subvi/oscilloscope/osci_autoset.vi"/>
				<Item Name="osci_get_histogram.vi" Type="VI" URL="../plugins_subvi/oscilloscope/osci_get_histogram.vi"/>
			</Item>
			<Item Name="playback" Type="Folder">
				<Item Name="build_files_tree.vi" Type="VI" URL="../plugins_subvi/song_playback/build_files_tree.vi"/>
				<Item Name="load_songfile.vi" Type="VI" URL="../plugins_subvi/song_playback/load_songfile.vi"/>
				<Item Name="playback.vi" Type="VI" URL="../plugins/playback.vi"/>
				<Item Name="ANNA_fileselectorbytime_v1.vi" Type="VI" URL="../plugins/beta quality/ANNA_fileselectorbytime_v1.vi"/>
				<Item Name="ANNA_buildtreefunc_v1.vi" Type="VI" URL="../plugins/beta quality/ANNA_buildtreefunc_v1.vi"/>
				<Item Name="ANNA_selectsubset_v1.vi" Type="VI" URL="../plugins/beta quality/ANNA_selectsubset_v1.vi"/>
				<Item Name="send_songfile.vi" Type="VI" URL="../plugins_subvi/song_playback/send_songfile.vi"/>
				<Item Name="load_send_songfile.vi" Type="VI" URL="../plugins_subvi/song_playback/load_send_songfile.vi"/>
				<Item Name="songfile_queue_load.vi" Type="VI" URL="../plugins_subvi/song_playback/songfile_queue_load.vi"/>
				<Item Name="songfile_queue_stream.vi" Type="VI" URL="../plugins_subvi/song_playback/songfile_queue_stream.vi"/>
				<Item Name="song playback.vit" Type="VI" URL="../plugins/song playback.vit"/>
			</Item>
			<Item Name="plot notifications" Type="Folder">
				<Item Name="activator_info.vi" Type="VI" URL="../plugins_subvi/plot notifications/activator_info.vi"/>
				<Item Name="add_new_notifications.vi" Type="VI" URL="../plugins_subvi/plot notifications/add_new_notifications.vi"/>
				<Item Name="build_a-d_tree.vi" Type="VI" URL="../plugins_subvi/plot notifications/build_a-d_tree.vi"/>
				<Item Name="data_files_paths.vi" Type="VI" URL="../plugins_subvi/plot notifications/data_files_paths.vi"/>
				<Item Name="info_fg.vi" Type="VI" URL="../plugins_subvi/plot notifications/info_fg.vi"/>
				<Item Name="load_notifications.vi" Type="VI" URL="../plugins_subvi/plot notifications/load_notifications.vi"/>
				<Item Name="make_graph.vi" Type="VI" URL="../plugins_subvi/plot notifications/make_graph.vi"/>
				<Item Name="options.ctl" Type="VI" URL="../plugins_subvi/plot notifications/options.ctl"/>
				<Item Name="options_fg.vi" Type="VI" URL="../plugins_subvi/plot notifications/options_fg.vi"/>
				<Item Name="options_popup.vi" Type="VI" URL="../plugins_subvi/plot notifications/options_popup.vi"/>
				<Item Name="plot_notification_stop_fg.vi" Type="VI" URL="../plugins_subvi/plot notifications/plot_notification_stop_fg.vi"/>
				<Item Name="plot notifications.vi" Type="VI" URL="../plugins/plot notifications.vi"/>
				<Item Name="plot_notifications_enum.ctl" Type="VI" URL="../plugins_subvi/plot notifications/plot_notifications_enum.ctl"/>
				<Item Name="save_notifications.vi" Type="VI" URL="../plugins_subvi/plot notifications/save_notifications.vi"/>
				<Item Name="update_fg.vi" Type="VI" URL="../plugins_subvi/plot notifications/update_fg.vi"/>
			</Item>
			<Item Name="remote_webcam" Type="Folder">
				<Item Name="webcam_remote.vi" Type="VI" URL="../plugins_subvi/remote_webcam/webcam_remote.vi"/>
				<Item Name="save_avi.vi" Type="VI" URL="../plugins_subvi/remote_webcam/save_avi.vi"/>
				<Item Name="request_avi_filename.vi" Type="VI" URL="../plugins_subvi/remote_webcam/request_avi_filename.vi"/>
				<Item Name="UDP_internal_notification_conversion.vi" Type="VI" URL="../plugins_subvi/UDP_webcam_get_send/UDP_internal_notification_conversion.vi"/>
			</Item>
			<Item Name="soundcard" Type="Folder">
				<Item Name="data_to_plot.vi" Type="VI" URL="../plugins_subvi/soundcard/data_to_plot.vi"/>
				<Item Name="filter_data.vi" Type="VI" URL="../plugins_subvi/soundcard/filter_data.vi"/>
				<Item Name="find_all_pecks.vi" Type="VI" URL="../plugins_subvi/soundcard/find_all_pecks.vi"/>
				<Item Name="last_peck.vi" Type="VI" URL="../plugins_subvi/soundcard/last_peck.vi"/>
				<Item Name="light_on_fg.vi" Type="VI" URL="../plugins_subvi/soundcard/light_on_fg.vi"/>
				<Item Name="make_graphs.vi" Type="VI" URL="../plugins_subvi/soundcard/make_graphs.vi"/>
				<Item Name="path_to_data.vi" Type="VI" URL="../plugins_subvi/soundcard/path_to_data.vi"/>
				<Item Name="peck_analysis_enum.ctl" Type="VI" URL="../plugins_subvi/soundcard/peck_analysis_enum.ctl"/>
				<Item Name="play_video_too.vi" Type="VI" URL="../plugins_subvi/soundcard/play_video_too.vi"/>
				<Item Name="play_wav.vi" Type="VI" URL="../plugins_subvi/soundcard/play_wav.vi"/>
				<Item Name="random_file_from_dir.vi" Type="VI" URL="../plugins_subvi/soundcard/random_file_from_dir.vi"/>
				<Item Name="scan_and_show_data.vi" Type="VI" URL="../plugins_subvi/soundcard/scan_and_show_data.vi"/>
				<Item Name="soundcard_enum.ctl" Type="VI" URL="../plugins_subvi/soundcard/soundcard_enum.ctl"/>
				<Item Name="soundcard.vi" Type="VI" URL="../plugins/soundcard.vi"/>
				<Item Name="stop_monitor_plugin.vi" Type="VI" URL="../plugins_subvi/soundcard/stop_monitor_plugin.vi"/>
				<Item Name="stop_soundcard_plugin.vi" Type="VI" URL="../plugins_subvi/soundcard/stop_soundcard_plugin.vi"/>
				<Item Name="string_to_timestamp.vi" Type="VI" URL="../plugins_subvi/soundcard/string_to_timestamp.vi"/>
				<Item Name="wait_ms.vi" Type="VI" URL="../plugins_subvi/soundcard/wait_ms.vi"/>
			</Item>
			<Item Name="trigger logger" Type="Folder">
				<Item Name="trigger logger.vi" Type="VI" URL="../plugins/trigger logger.vi"/>
				<Item Name="write_trigger_to_file.vi" Type="VI" URL="../plugins_subvi/trigger_logger/write_trigger_to_file.vi"/>
				<Item Name="get_trigger_filename.vi" Type="VI" URL="../plugins_subvi/trigger_logger/get_trigger_filename.vi"/>
			</Item>
			<Item Name="UDP michi" Type="Folder">
				<Item Name="UDP webcam michi.vi" Type="VI" URL="../plugins/UDP webcam michi.vi"/>
				<Item Name="UDP webcam michi send.vi" Type="VI" URL="../plugins/UDP webcam michi send.vi"/>
			</Item>
			<Item Name="UDP remote client" Type="Folder">
				<Item Name="UDP_client.vi" Type="VI" URL="../plugins_subvi/UDP_client/UDP_client.vi"/>
				<Item Name="UDP_client_get.vi" Type="VI" URL="../plugins_subvi/UDP_client/UDP_client_get.vi"/>
				<Item Name="UDP_client_init.vi" Type="VI" URL="../plugins_subvi/UDP_client/UDP_client_init.vi"/>
				<Item Name="UDP_client_notifiers_get.vi" Type="VI" URL="../plugins_subvi/UDP_client/UDP_client_notifiers_get.vi"/>
				<Item Name="UDP_client_send.vi" Type="VI" URL="../plugins_subvi/UDP_client/UDP_client_send.vi"/>
				<Item Name="UDP_client_stop.vi" Type="VI" URL="../plugins_subvi/UDP_client/UDP_client_stop.vi"/>
			</Item>
			<Item Name="UDP webcam" Type="Folder">
				<Item Name="stop_UDP_webcam.vi" Type="VI" URL="../plugins_subvi/UDP webcam/stop_UDP_webcam.vi"/>
				<Item Name="UDP webcam.vit" Type="VI" URL="../plugins/UDP webcam.vit"/>
				<Item Name="UDP_avi_filename_send.vi" Type="VI" URL="../plugins_subvi/UDP webcam/UDP_avi_filename_send.vi"/>
				<Item Name="UDP_motion_level_set.vi" Type="VI" URL="../plugins_subvi/UDP webcam/UDP_motion_level_set.vi"/>
				<Item Name="UDP_remote_port_get.vi" Type="VI" URL="../plugins_subvi/UDP webcam/UDP_remote_port_get.vi"/>
				<Item Name="UDP_email_warning.vi" Type="VI" URL="../plugins_subvi/UDP webcam/UDP_email_warning.vi"/>
			</Item>
			<Item Name="UDP webcam get send" Type="Folder">
				<Item Name="UDP get send.vi" Type="VI" URL="../plugins/UDP get send.vi"/>
				<Item Name="stop_UDP_get_send.vi" Type="VI" URL="../plugins_subvi/UDP_webcam_get_send/stop_UDP_get_send.vi"/>
				<Item Name="UDP_detector_level_set.vi" Type="VI" URL="../plugins_subvi/UDP_webcam_get_send/UDP_detector_level_set.vi"/>
				<Item Name="UDP_activator_notification_send.vi" Type="VI" URL="../plugins_subvi/UDP_webcam_get_send/UDP_activator_notification_send.vi"/>
				<Item Name="UDP_answer_avi_request.vi" Type="VI" URL="../plugins_subvi/UDP_webcam_get_send/UDP_answer_avi_request.vi"/>
			</Item>
			<Item Name="TwoPhotonMic" Type="Folder">
				<Item Name="voltage2laserpower.vi" Type="VI" URL="../plugins/voltage2laserpower.vi"/>
			</Item>
			<Item Name="helpers" Type="Folder">
				<Item Name="plugin_reg_message_log.vi" Type="VI" URL="../subvi/plugins/plugin_reg_message_log.vi"/>
				<Item Name="plugin_unreg_message_log.vi" Type="VI" URL="../subvi/plugins/plugin_unreg_message_log.vi"/>
				<Item Name="run_plugin.vi" Type="VI" URL="../subvi/plugins/run_plugin.vi"/>
				<Item Name="un_register_plugin.vi" Type="VI" URL="../subvi/plugins/un_register_plugin.vi"/>
				<Item Name="list_plugins.vi" Type="VI" URL="../subvi/plugins/list_plugins.vi"/>
			</Item>
			<Item Name="collision test new.vi" Type="VI" URL="../plugins/collision test new.vi"/>
			<Item Name="collision test.vi" Type="VI" URL="../plugins/collision test.vi"/>
			<Item Name="effector interface.vi" Type="VI" URL="../plugins/effector interface.vi"/>
			<Item Name="get_plugin_notifications.vi" Type="VI" URL="../subvi/DAE_loop/get_plugin_notifications.vi"/>
			<Item Name="plugin_notifiers.vi" Type="VI" URL="../fgs/plugin_notifiers.vi"/>
			<Item Name="write digital channel.vi" Type="VI" URL="../plugins/write digital channel.vi"/>
			<Item Name="plugin_template.vi" Type="VI" URL="../plugins/plugin_template.vi"/>
			<Item Name="poisson stim interface.vi" Type="VI" URL="../plugins/poisson stim interface.vi"/>
			<Item Name="tdms_properties.vi" Type="VI" URL="../plugins/tdms_properties.vi"/>
			<Item Name="tdms_properties_get.vi" Type="VI" URL="../plugins/tdms_properties_get.vi"/>
		</Item>
		<Item Name="run time menu" Type="Folder">
			<Item Name="edit_run_time_menu.vi" Type="VI" URL="../run-time menu/edit_run_time_menu.vi"/>
			<Item Name="ItemTag_function.vi" Type="VI" URL="../run-time menu/ItemTag_function.vi"/>
			<Item Name="menu_D-A-E.vi" Type="VI" URL="../run-time menu/menu_D-A-E.vi"/>
			<Item Name="menu_help.vi" Type="VI" URL="../run-time menu/menu_help.vi"/>
			<Item Name="menu_plugins.vi" Type="VI" URL="../run-time menu/menu_plugins.vi"/>
			<Item Name="menu_settings.vi" Type="VI" URL="../run-time menu/menu_settings.vi"/>
			<Item Name="menu_setups.vi" Type="VI" URL="../run-time menu/menu_setups.vi"/>
			<Item Name="run_time_menu.rtm" Type="Document" URL="../run-time menu/run_time_menu.rtm"/>
		</Item>
		<Item Name="subvi" Type="Folder">
			<Item Name="DAE_loop" Type="Folder">
				<Item Name="detect_effect_loop.vi" Type="VI" URL="../subvi/DAE_loop/detect_effect_loop.vi"/>
				<Item Name="save_select_channels.vi" Type="VI" URL="../subvi/DAE_loop/save_select_channels.vi"/>
			</Item>
			<Item Name="DAQ_loop" Type="Folder">
				<Item Name="DAQ_AIAO_loop.vi" Type="VI" URL="../subvi/DAQ_loop/DAQ_AIAO_loop.vi"/>
				<Item Name="DAQ_start.vi" Type="VI" URL="../subvi/DAQ_loop/DAQ_start.vi"/>
				<Item Name="get_lvd_filenames.vi" Type="VI" URL="../subvi/DAQ_loop/get_lvd_filenames.vi"/>
				<Item Name="scale_scxi.vi" Type="VI" URL="../subvi/DAQ_loop/scale_scxi.vi"/>
			</Item>
			<Item Name="event_loop" Type="Folder">
				<Item Name="stop_and_stop_plugins.vi" Type="VI" URL="../subvi/event_loop/stop_and_stop_plugins.vi"/>
				<Item Name="edit_d-a-e.vi" Type="VI" URL="../subvi/event_loop/edit_d-a-e.vi"/>
			</Item>
			<Item Name="events" Type="Folder">
				<Item Name="user_events.ctl" Type="VI" URL="../controls/user_events.ctl"/>
				<Item Name="events.vi" Type="VI" URL="../subvi/events/events.vi"/>
			</Item>
			<Item Name="helpers" Type="Folder">
				<Item Name="autocorr.vi" Type="VI" URL="../subvi/helpers/autocorr.vi"/>
				<Item Name="double_to_timestamp.vi" Type="VI" URL="../subvi/helpers/double_to_timestamp.vi"/>
				<Item Name="ftfil.vi" Type="VI" URL="../subvi/helpers/ftfil.vi"/>
				<Item Name="load_labview_chronic.vi" Type="VI" URL="../subvi/helpers/load_labview_chronic.vi"/>
				<Item Name="lvd_get_num_samples.vi" Type="VI" URL="../subvi/helpers/lvd_get_num_samples.vi"/>
				<Item Name="rand_exp.vi" Type="VI" URL="../subvi/helpers/rand_exp.vi"/>
				<Item Name="resample_frequency.vi" Type="VI" URL="../subvi/helpers/resample_frequency.vi"/>
				<Item Name="string_to_array.vi" Type="VI" URL="../subvi/helpers/string_to_array.vi"/>
				<Item Name="tan_sig.vi" Type="VI" URL="../subvi/helpers/tan_sig.vi"/>
				<Item Name="timestamp2matlab_time.vi" Type="VI" URL="../subvi/helpers/timestamp2matlab_time.vi"/>
				<Item Name="timestamp2seconds_today.vi" Type="VI" URL="../subvi/helpers/timestamp2seconds_today.vi"/>
				<Item Name="warningNOwait.vi" Type="VI" URL="../subvi/helpers/warningNOwait.vi"/>
				<Item Name="warningNOwait.vit" Type="VI" URL="../subvi/helpers/warningNOwait.vit"/>
				<Item Name="warningNOwait_fg.vi" Type="VI" URL="../subvi/helpers/warningNOwait_fg.vi"/>
				<Item Name="percentile.vi" Type="VI" URL="../subvi/helpers/percentile.vi"/>
			</Item>
			<Item Name="initialization" Type="Folder">
				<Item Name="check_settings.vi" Type="VI" URL="../subvi/initialization/check_settings.vi"/>
				<Item Name="get_physical_channel_array.vi" Type="VI" URL="../subvi/initialization/get_physical_channel_array.vi"/>
				<Item Name="init_AI.vi" Type="VI" URL="../subvi/initialization/init_AI.vi"/>
				<Item Name="init_AI_channel#.vi" Type="VI" URL="../subvi/initialization/init_AI_channel#.vi"/>
				<Item Name="init_AO.vi" Type="VI" URL="../subvi/initialization/init_AO.vi"/>
				<Item Name="init_d-a-e.vi" Type="VI" URL="../subvi/initialization/init_d-a-e.vi"/>
				<Item Name="init_DAQ.vi" Type="VI" URL="../subvi/initialization/init_DAQ.vi"/>
				<Item Name="init_various.vi" Type="VI" URL="../subvi/initialization/init_various.vi"/>
				<Item Name="save_indices_AIAO_get.vi" Type="VI" URL="../subvi/initialization/save_indices_AIAO_get.vi"/>
				<Item Name="setup_of_save_detector.vi" Type="VI" URL="../subvi/initialization/setup_of_save_detector.vi"/>
				<Item Name="init_DI.vi" Type="VI" URL="../subvi/initialization/init_DI.vi"/>
				<Item Name="init_DO.vi" Type="VI" URL="../subvi/initialization/init_DO.vi"/>
			</Item>
			<Item Name="observation_panel" Type="Folder">
				<Item Name="activator_info_cluster.vi" Type="VI" URL="../subvi/observation_panel/activator_info_cluster.vi"/>
				<Item Name="detector_info_cluster.vi" Type="VI" URL="../subvi/observation_panel/detector_info_cluster.vi"/>
				<Item Name="saving_title_get.vi" Type="VI" URL="../subvi/observation_panel/saving_title_get.vi"/>
				<Item Name="update_chart_buffer.vi" Type="VI" URL="../subvi/observation_panel/update_chart_buffer.vi"/>
				<Item Name="observation_panel.vi" Type="VI" URL="../subvi/observation_panel/observation_panel.vi"/>
			</Item>
			<Item Name="report_loop" Type="Folder">
				<Item Name="gmail.vi" Type="VI" URL="../email/gmail.vi"/>
				<Item Name="report_generator.vi" Type="VI" URL="../subvi/report_loop/report_generator.vi"/>
				<Item Name="create_daily_summary.vi" Type="VI" URL="../subvi/report_loop/create_daily_summary.vi"/>
				<Item Name="gethostname.vi" Type="VI" URL="../subvi/report_loop/gethostname.vi"/>
				<Item Name="HDfull_email.vi" Type="VI" URL="../subvi/report_loop/HDfull_email.vi"/>
			</Item>
			<Item Name="save_loop" Type="Folder">
				<Item Name="close_file_references.vi" Type="VI" URL="../subvi/save_loop/close_file_references.vi"/>
				<Item Name="count_post_trigger_samples.vi" Type="VI" URL="../subvi/save_loop/count_post_trigger_samples.vi"/>
				<Item Name="count_pre-post-samples.vi" Type="VI" URL="../subvi/save_loop/count_pre-post-samples.vi"/>
				<Item Name="create_meta_file_reference.vi" Type="VI" URL="../subvi/save_loop/create_meta_file_reference.vi"/>
				<Item Name="create_wav_file_reference.vi" Type="VI" URL="../subvi/save_loop/create_wav_file_reference.vi"/>
				<Item Name="file_type_string.vi" Type="VI" URL="../subvi/save_loop/file_type_string.vi"/>
				<Item Name="save_buffer.vi" Type="VI" URL="../subvi/save_loop/save_buffer.vi"/>
				<Item Name="save_buffer_to_setup.vi" Type="VI" URL="../subvi/save_loop/save_buffer_to_setup.vi"/>
				<Item Name="save_check_simulated_file_end.vi" Type="VI" URL="../subvi/save_loop/save_check_simulated_file_end.vi"/>
				<Item Name="save_create_file.vi" Type="VI" URL="../subvi/save_loop/save_create_file.vi"/>
				<Item Name="save_create_filename.vi" Type="VI" URL="../subvi/save_loop/save_create_filename.vi"/>
				<Item Name="save_data_folder_get.vi" Type="VI" URL="../subvi/save_loop/save_data_folder_get.vi"/>
				<Item Name="save_datapath_get.vi" Type="VI" URL="../subvi/save_loop/save_datapath_get.vi"/>
				<Item Name="save_enqueue_trigger.vi" Type="VI" URL="../subvi/save_loop/save_enqueue_trigger.vi"/>
				<Item Name="save_filename_saving_get.vi" Type="VI" URL="../subvi/save_loop/save_filename_saving_get.vi"/>
				<Item Name="save_filetype_get_set.vi" Type="VI" URL="../subvi/save_loop/save_filetype_get_set.vi"/>
				<Item Name="save_folder_create.vi" Type="VI" URL="../subvi/save_loop/save_folder_create.vi"/>
				<Item Name="save_message_type.vi" Type="VI" URL="../subvi/save_loop/save_message_type.vi"/>
				<Item Name="save_meta_file.vi" Type="VI" URL="../subvi/save_loop/save_meta_file.vi"/>
				<Item Name="save_meta_file_activator_threshold.vi" Type="VI" URL="../subvi/save_loop/save_meta_file_activator_threshold.vi"/>
				<Item Name="save_meta_file_d-a-e_parameters.vi" Type="VI" URL="../subvi/save_loop/save_meta_file_d-a-e_parameters.vi"/>
				<Item Name="save_meta_file_get_triggers.vi" Type="VI" URL="../subvi/save_loop/save_meta_file_get_triggers.vi"/>
				<Item Name="save_meta_file_sort_triggers.vi" Type="VI" URL="../subvi/save_loop/save_meta_file_sort_triggers.vi"/>
				<Item Name="save_meta_file_trigger_activator_msg.vi" Type="VI" URL="../subvi/save_loop/save_meta_file_trigger_activator_msg.vi"/>
				<Item Name="save_meta_file_trigger_message.vi" Type="VI" URL="../subvi/save_loop/save_meta_file_trigger_message.vi"/>
				<Item Name="save_meta_file_triggers_to_string.vi" Type="VI" URL="../subvi/save_loop/save_meta_file_triggers_to_string.vi"/>
				<Item Name="save_multisetup_init_basics.vi" Type="VI" URL="../subvi/save_loop/save_multisetup_init_basics.vi"/>
				<Item Name="save_multisetup_loop.vi" Type="VI" URL="../subvi/save_loop/save_multisetup_loop.vi"/>
				<Item Name="save_observation_get_set.vi" Type="VI" URL="../subvi/save_loop/save_observation_get_set.vi"/>
				<Item Name="save_set_first_file_#.vi" Type="VI" URL="../subvi/save_loop/save_set_first_file_#.vi"/>
				<Item Name="save_trigger_to_save.vi" Type="VI" URL="../subvi/save_loop/save_trigger_to_save.vi"/>
				<Item Name="save_write_buffer.vi" Type="VI" URL="../subvi/save_loop/save_write_buffer.vi"/>
			</Item>
			<Item Name="settings" Type="Folder">
				<Item Name="recoorder_settings_txt.vi" Type="VI" URL="../subvi/settings/recoorder_settings_txt.vi"/>
				<Item Name="recorder_settings_setup_description.vi" Type="VI" URL="../subvi/settings/recorder_settings_setup_description.vi"/>
			</Item>
			<Item Name="UDP server" Type="Folder">
				<Item Name="UDP_server_get.vi" Type="VI" URL="../subvi/UDP server/UDP_server_get.vi"/>
				<Item Name="UDP_server_send.vi" Type="VI" URL="../subvi/UDP server/UDP_server_send.vi"/>
				<Item Name="UDP_server.vi" Type="VI" URL="../subvi/UDP server/UDP_server.vi"/>
				<Item Name="local_IP.vi" Type="VI" URL="../subvi/UDP/local_IP.vi"/>
				<Item Name="UDP_notification_out_get.vi" Type="VI" URL="../subvi/UDP server/UDP_notification_out_get.vi"/>
				<Item Name="UDP_notification_in_get.vi" Type="VI" URL="../subvi/UDP server/UDP_notification_in_get.vi"/>
				<Item Name="checksum.vi" Type="VI" URL="../subvi/UDP server/checksum.vi"/>
				<Item Name="port2string.vi" Type="VI" URL="../subvi/UDP server/port2string.vi"/>
				<Item Name="string2port.vi" Type="VI" URL="../subvi/UDP server/string2port.vi"/>
			</Item>
			<Item Name="update_DAE_loop" Type="Folder">
				<Item Name="update_d-a-e.vi" Type="VI" URL="../subvi/update_d-a-e_loop/update_d-a-e.vi"/>
				<Item Name="stop_update_d-a-e.vi" Type="VI" URL="../subvi/update_d-a-e_loop/stop_update_d-a-e.vi"/>
			</Item>
			<Item Name="array indexing" Type="Folder">
				<Item Name="find.vi" Type="VI" URL="../subvi/array indexing/find.vi"/>
				<Item Name="subset_by_index.vi" Type="VI" URL="../subvi/array indexing/subset_by_index.vi"/>
				<Item Name="logical2indices.vi" Type="VI" URL="../subvi/array indexing/logical2indices.vi"/>
				<Item Name="unique.vi" Type="VI" URL="../subvi/array indexing/unique.vi"/>
				<Item Name="diff.vi" Type="VI" URL="../subvi/array indexing/diff.vi"/>
			</Item>
			<Item Name="activator_tag_txt.vi" Type="VI" URL="../subvi/save_loop/activator_tag_txt.vi"/>
			<Item Name="channels_per_type.vi" Type="VI" URL="../fgs/channels_per_type.vi"/>
			<Item Name="check_setups_and_channels.vi" Type="VI" URL="../subvi/check_setups_and_channels.vi"/>
			<Item Name="clear_queues.vi" Type="VI" URL="../subvi/clear_queues.vi"/>
			<Item Name="colormap_jet.vi" Type="VI" URL="../subvi/helpers/colormap_jet.vi"/>
			<Item Name="load_settings_trigger.vi" Type="VI" URL="../subvi/load_settings_trigger.vi"/>
			<Item Name="recorder_start_up.vi" Type="VI" URL="../subvi/recorder_start_up.vi"/>
			<Item Name="recorder_startup_message.vi" Type="VI" URL="../subvi/recorder_startup_message.vi"/>
			<Item Name="restart_or_shut_down.vi" Type="VI" URL="../subvi/restart_or_shut_down.vi"/>
			<Item Name="save_load_settings.vi" Type="VI" URL="../subvi/save_loop/save_load_settings.vi"/>
			<Item Name="save_meta_file_plugin_notif_to_string.vi" Type="VI" URL="../subvi/save_loop/save_meta_file_plugin_notif_to_string.vi"/>
			<Item Name="send_recorder_email.vi" Type="VI" URL="../subvi/send_recorder_email.vi"/>
			<Item Name="status_bar_message_get.vi" Type="VI" URL="../subvi/status_bar_message_get.vi"/>
			<Item Name="wrap_d-a-e.vi" Type="VI" URL="../subvi/initialization/wrap_d-a-e.vi"/>
			<Item Name="write_error_message_to_disk.vi" Type="VI" URL="../subvi/write_error_message_to_disk.vi"/>
			<Item Name="write_logfile_to_disk.vi" Type="VI" URL="../subvi/write_logfile_to_disk.vi"/>
		</Item>
		<Item Name="templates" Type="Folder">
			<Item Name="init_wait_check_execute.vi" Type="VI" URL="../templates/init_wait_check_execute.vi"/>
		</Item>
		<Item Name="transform_classes" Type="Folder">
			<Item Name="transform.lvclass" Type="LVClass" URL="../classes/transform_class/transform/transform.lvclass"/>
			<Item Name="transform_autocorr.lvclass" Type="LVClass" URL="../classes/transform_class/transform_autocorr/transform_autocorr.lvclass"/>
			<Item Name="transform_fft_hamming.lvclass" Type="LVClass" URL="../classes/transform_class/transform_fft_hamming/transform_fft_hamming.lvclass"/>
			<Item Name="transform_powerspectrum.lvclass" Type="LVClass" URL="../classes/transform_class/transform_powerspectrum/transform_powerspectrum.lvclass"/>
			<Item Name="transform_raw.lvclass" Type="LVClass" URL="../classes/transform_class/transform_raw/transform_raw.lvclass"/>
			<Item Name="transform_mode.ctl" Type="VI" URL="../classes/transform_class/transform_mode.ctl"/>
			<Item Name="get_transform_class.vi" Type="VI" URL="../classes/transform_class/get_transform_class.vi"/>
			<Item Name="run_transforms.vi" Type="VI" URL="../classes/transform_class/run_transforms.vi"/>
			<Item Name="transform_filter.lvclass" Type="LVClass" URL="../classes/transform_class/transform_filter/transform_filter.lvclass"/>
		</Item>
		<Item Name="help" Type="Folder">
			<Item Name="help.vi" Type="VI" URL="../subvi/help/help.vi"/>
			<Item Name="help_build_item_tree.vi" Type="VI" URL="../subvi/help/help_build_item_tree.vi"/>
			<Item Name="help_fill_RichTextBox.vi" Type="VI" URL="../subvi/help/help_fill_RichTextBox.vi"/>
			<Item Name="get_class_help_files.vi" Type="VI" URL="../subvi/help/get_class_help_files.vi"/>
		</Item>
		<Item Name="channels" Type="Folder">
			<Item Name="channel.lvclass" Type="LVClass" URL="../classes/channel/channel.lvclass"/>
			<Item Name="channel_AI.lvclass" Type="LVClass" URL="../classes/channel/channel_AI/channel_AI.lvclass"/>
			<Item Name="channel_AO.lvclass" Type="LVClass" URL="../classes/channel/channel_AO/channel_AO.lvclass"/>
			<Item Name="channel_DI.lvclass" Type="LVClass" URL="../classes/channel/channel_DI/channel_DI.lvclass"/>
			<Item Name="channel_DO.lvclass" Type="LVClass" URL="../classes/channel/channel_DO/channel_DO.lvclass"/>
		</Item>
		<Item Name="setups" Type="Folder">
			<Item Name="setup.lvclass" Type="LVClass" URL="../classes/setup/setup.lvclass"/>
		</Item>
		<Item Name="global variables" Type="Folder">
			<Item Name="para_global.vi" Type="VI" URL="../global_variables/para_global.vi"/>
			<Item Name="states_global.vi" Type="VI" URL="../global_variables/states_global.vi"/>
			<Item Name="skipped_global.vi" Type="VI" URL="../global_variables/skipped_global.vi"/>
		</Item>
		<Item Name="documentation_How_To" Type="Folder">
			<Item Name="How_To_Interact_With_DAE.rtf" Type="Document" URL="../docu/How_To_Interact_With_DAE.rtf"/>
			<Item Name="How_to_Write_A_Plugin.rtf" Type="Document" URL="../docu/How_to_Write_A_Plugin.rtf"/>
			<Item Name="How_to_Write_Activator_With_Mathscript.rtf" Type="Document" URL="../docu/How_to_Write_Activator_With_Mathscript.rtf"/>
		</Item>
		<Item Name="configure_detectors_activators_effectors.lvsc" Type="LVStatechart" URL="../statecharts/configure_detectors_activators_effectors.lvsc"/>
		<Item Name="update_detectors_activators_effectors.lvsc" Type="LVStatechart" URL="../statecharts/update_detectors_activators_effectors.lvsc"/>
		<Item Name="configure_setups_channels.lvsc" Type="LVStatechart" URL="../statecharts/configure_setups_channels.lvsc"/>
		<Item Name="finch16x16.ico" Type="Document" URL="../finch16x16.ico"/>
		<Item Name="recoorder.vi" Type="VI" URL="../recoorder.vi"/>
		<Item Name="recorder_settings.ini" Type="Document" URL="../recorder_settings.ini"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
				<Item Name="Check if File or Folder Exists.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Check if File or Folder Exists.vi"/>
				<Item Name="NI_FileType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/lvfile.llb/NI_FileType.lvlib"/>
				<Item Name="DAQmx Fill In Error Info.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/miscellaneous.llb/DAQmx Fill In Error Info.vi"/>
				<Item Name="DAQmx Write (Analog 2D DBL NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog 2D DBL NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Digital 1D Bool 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D Bool 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital 2D U8 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 2D U8 NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Analog 1D DBL 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog 1D DBL 1Chan NSamp).vi"/>
				<Item Name="DAQmx Write.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write.vi"/>
				<Item Name="DAQmx Read (Digital 2D U8 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 2D U8 NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Analog 2D DBL NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 2D DBL NChan NSamp).vi"/>
				<Item Name="DAQmx Read.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read.vi"/>
				<Item Name="DAQmx Clear Task.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/task.llb/DAQmx Clear Task.vi"/>
				<Item Name="DAQmx Timing.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/timing.llb/DAQmx Timing.vi"/>
				<Item Name="DAQmx Timing (Sample Clock).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/timing.llb/DAQmx Timing (Sample Clock).vi"/>
				<Item Name="DAQmx Timing (Implicit).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/timing.llb/DAQmx Timing (Implicit).vi"/>
				<Item Name="DAQmx Create Virtual Channel.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Virtual Channel.vi"/>
				<Item Name="DAQmx Create Channel (AI-Voltage-Basic).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Voltage-Basic).vi"/>
				<Item Name="DAQmx Create Channel (AO-Voltage-Basic).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AO-Voltage-Basic).vi"/>
				<Item Name="DAQmx Create Channel (DI-Digital Input).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (DI-Digital Input).vi"/>
				<Item Name="DAQmx Create Channel (DO-Digital Output).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (DO-Digital Output).vi"/>
				<Item Name="DAQmx Create Channel (CO-Pulse Generation-Frequency).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CO-Pulse Generation-Frequency).vi"/>
				<Item Name="DAQmx Create Channel (CO-Pulse Generation-Time).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CO-Pulse Generation-Time).vi"/>
				<Item Name="DAQmx Start Task.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/task.llb/DAQmx Start Task.vi"/>
				<Item Name="NI_AALPro.lvlib" Type="Library" URL="/&lt;vilib&gt;/Analysis/NI_AALPro.lvlib"/>
				<Item Name="Sound File Read Simple.vi" Type="VI" URL="/&lt;vilib&gt;/sound2/lvsound2.llb/Sound File Read Simple.vi"/>
				<Item Name="Sound File Position.ctl" Type="VI" URL="/&lt;vilib&gt;/sound2/lvsound2.llb/Sound File Position.ctl"/>
				<Item Name="Sound File Close.vi" Type="VI" URL="/&lt;vilib&gt;/sound2/lvsound2.llb/Sound File Close.vi"/>
				<Item Name="Sound File Refnum.ctl" Type="VI" URL="/&lt;vilib&gt;/sound2/lvsound2.llb/Sound File Refnum.ctl"/>
				<Item Name="_Get Sound Error From Return Value.vi" Type="VI" URL="/&lt;vilib&gt;/sound2/lvsound2.llb/_Get Sound Error From Return Value.vi"/>
				<Item Name="Sound File Read (DBL).vi" Type="VI" URL="/&lt;vilib&gt;/sound2/lvsound2.llb/Sound File Read (DBL).vi"/>
				<Item Name="Sound File Info (refnum).vi" Type="VI" URL="/&lt;vilib&gt;/sound2/lvsound2.llb/Sound File Info (refnum).vi"/>
				<Item Name="Sound Data Format.ctl" Type="VI" URL="/&lt;vilib&gt;/sound2/lvsound2.llb/Sound Data Format.ctl"/>
				<Item Name="_2DArrToArrWfms.vi" Type="VI" URL="/&lt;vilib&gt;/sound2/lvsound2.llb/_2DArrToArrWfms.vi"/>
				<Item Name="Sound File Read Open.vi" Type="VI" URL="/&lt;vilib&gt;/sound2/lvsound2.llb/Sound File Read Open.vi"/>
				<Item Name="Path To Command Line String.vi" Type="VI" URL="/&lt;vilib&gt;/AdvancedString/Path To Command Line String.vi"/>
				<Item Name="PathToUNIXPathString.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/CFURL.llb/PathToUNIXPathString.vi"/>
				<Item Name="NI_Gmath.lvlib" Type="Library" URL="/&lt;vilib&gt;/gmath/NI_Gmath.lvlib"/>
				<Item Name="NI_AALBase.lvlib" Type="Library" URL="/&lt;vilib&gt;/Analysis/NI_AALBase.lvlib"/>
				<Item Name="LVRangeTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRangeTypeDef.ctl"/>
				<Item Name="Space Constant.vi" Type="VI" URL="/&lt;vilib&gt;/dlg_ctls.llb/Space Constant.vi"/>
				<Item Name="Read From Spreadsheet File.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read From Spreadsheet File.vi"/>
				<Item Name="Read From Spreadsheet File (DBL).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read From Spreadsheet File (DBL).vi"/>
				<Item Name="Read Lines From File.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read Lines From File.vi"/>
				<Item Name="DialogType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogType.ctl"/>
				<Item Name="General Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler.vi"/>
				<Item Name="DialogTypeEnum.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogTypeEnum.ctl"/>
				<Item Name="Check Special Tags.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Check Special Tags.vi"/>
				<Item Name="TagReturnType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/TagReturnType.ctl"/>
				<Item Name="Set String Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set String Value.vi"/>
				<Item Name="GetRTHostConnectedProp.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetRTHostConnectedProp.vi"/>
				<Item Name="Error Code Database.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Code Database.vi"/>
				<Item Name="Format Message String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Format Message String.vi"/>
				<Item Name="Find Tag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find Tag.vi"/>
				<Item Name="Search and Replace Pattern.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Search and Replace Pattern.vi"/>
				<Item Name="Set Bold Text.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set Bold Text.vi"/>
				<Item Name="Details Display Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Details Display Dialog.vi"/>
				<Item Name="ErrWarn.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/ErrWarn.ctl"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="eventvkey.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/eventvkey.ctl"/>
				<Item Name="Not Found Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Not Found Dialog.vi"/>
				<Item Name="Three Button Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog.vi"/>
				<Item Name="Three Button Dialog CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog CORE.vi"/>
				<Item Name="Longest Line Length in Pixels.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Longest Line Length in Pixels.vi"/>
				<Item Name="Convert property node font to graphics font.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Convert property node font to graphics font.vi"/>
				<Item Name="Get Text Rect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Get Text Rect.vi"/>
				<Item Name="Get String Text Bounds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Get String Text Bounds.vi"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
				<Item Name="BuildHelpPath.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/BuildHelpPath.vi"/>
				<Item Name="GetHelpDir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetHelpDir.vi"/>
				<Item Name="Close File+.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Close File+.vi"/>
				<Item Name="Find First Error.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find First Error.vi"/>
				<Item Name="Read File+ (string).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read File+ (string).vi"/>
				<Item Name="compatReadText.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatReadText.vi"/>
				<Item Name="Open File+.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Open File+.vi"/>
				<Item Name="Read From Spreadsheet File (I64).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read From Spreadsheet File (I64).vi"/>
				<Item Name="Read From Spreadsheet File (string).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read From Spreadsheet File (string).vi"/>
				<Item Name="Simple Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Simple Error Handler.vi"/>
				<Item Name="LVKeyTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVKeyTypeDef.ctl"/>
				<Item Name="VISA Configure Serial Port" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Configure Serial Port"/>
				<Item Name="VISA Configure Serial Port (Instr).vi" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Configure Serial Port (Instr).vi"/>
				<Item Name="VISA Configure Serial Port (Serial Instr).vi" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Configure Serial Port (Serial Instr).vi"/>
				<Item Name="VISA Flush IO Buffer Mask.ctl" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Flush IO Buffer Mask.ctl"/>
				<Item Name="DAQmx Is Task Done.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/task.llb/DAQmx Is Task Done.vi"/>
				<Item Name="DAQmx Wait Until Done.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/task.llb/DAQmx Wait Until Done.vi"/>
				<Item Name="Normalize Waveform.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/Normalize Waveform.vi"/>
				<Item Name="DWDT Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DWDTOps.llb/DWDT Error Code.vi"/>
				<Item Name="DAQmx Stop Task.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/task.llb/DAQmx Stop Task.vi"/>
				<Item Name="Image Type" Type="VI" URL="/&lt;vilib&gt;/vision/Image Controls.llb/Image Type"/>
				<Item Name="IMAQ Dispose" Type="VI" URL="/&lt;vilib&gt;/vision/Basics.llb/IMAQ Dispose"/>
				<Item Name="IMAQ Create" Type="VI" URL="/&lt;vilib&gt;/vision/Basics.llb/IMAQ Create"/>
				<Item Name="IMAQ AVI Get Filter Names" Type="VI" URL="/&lt;vilib&gt;/vision/Avi2.llb/IMAQ AVI Get Filter Names"/>
				<Item Name="IMAQ ColorImageToArray" Type="VI" URL="/&lt;vilib&gt;/vision/Basics.llb/IMAQ ColorImageToArray"/>
				<Item Name="IMAQ AVI Close" Type="VI" URL="/&lt;vilib&gt;/vision/Avi1.llb/IMAQ AVI Close"/>
				<Item Name="IMAQ AVI Create" Type="VI" URL="/&lt;vilib&gt;/vision/Avi1.llb/IMAQ AVI Create"/>
				<Item Name="IMAQ AVI Write Frame" Type="VI" URL="/&lt;vilib&gt;/vision/Avi1.llb/IMAQ AVI Write Frame"/>
				<Item Name="IMAQ ArrayToColorImage" Type="VI" URL="/&lt;vilib&gt;/vision/Basics.llb/IMAQ ArrayToColorImage"/>
				<Item Name="IMAQdx.ctl" Type="VI" URL="/&lt;vilib&gt;/userDefined/High Color/IMAQdx.ctl"/>
				<Item Name="Get File Extension.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Get File Extension.vi"/>
				<Item Name="Sound File Open.vi" Type="VI" URL="/&lt;vilib&gt;/sound2/lvsound2.llb/Sound File Open.vi"/>
				<Item Name="Sound File Write Open.vi" Type="VI" URL="/&lt;vilib&gt;/sound2/lvsound2.llb/Sound File Write Open.vi"/>
				<Item Name="Sound File Write.vi" Type="VI" URL="/&lt;vilib&gt;/sound2/lvsound2.llb/Sound File Write.vi"/>
				<Item Name="Sound File Write (I16).vi" Type="VI" URL="/&lt;vilib&gt;/sound2/lvsound2.llb/Sound File Write (I16).vi"/>
				<Item Name="_ArrWfmsToData.vi" Type="VI" URL="/&lt;vilib&gt;/sound2/lvsound2.llb/_ArrWfmsToData.vi"/>
				<Item Name="_ArrWfmsTo1DInterleave.vi" Type="VI" URL="/&lt;vilib&gt;/sound2/lvsound2.llb/_ArrWfmsTo1DInterleave.vi"/>
				<Item Name="_ArrWfmsTo2DArr.vi" Type="VI" URL="/&lt;vilib&gt;/sound2/lvsound2.llb/_ArrWfmsTo2DArr.vi"/>
				<Item Name="Sound File Write (DBL).vi" Type="VI" URL="/&lt;vilib&gt;/sound2/lvsound2.llb/Sound File Write (DBL).vi"/>
				<Item Name="Sound File Write (I32).vi" Type="VI" URL="/&lt;vilib&gt;/sound2/lvsound2.llb/Sound File Write (I32).vi"/>
				<Item Name="Sound File Write (SGL).vi" Type="VI" URL="/&lt;vilib&gt;/sound2/lvsound2.llb/Sound File Write (SGL).vi"/>
				<Item Name="Sound File Write (U8).vi" Type="VI" URL="/&lt;vilib&gt;/sound2/lvsound2.llb/Sound File Write (U8).vi"/>
				<Item Name="Sound File Write (DBL Single).vi" Type="VI" URL="/&lt;vilib&gt;/sound2/lvsound2.llb/Sound File Write (DBL Single).vi"/>
				<Item Name="Sound Output Wait.vi" Type="VI" URL="/&lt;vilib&gt;/sound2/lvsound2.llb/Sound Output Wait.vi"/>
				<Item Name="Sound Output Task ID.ctl" Type="VI" URL="/&lt;vilib&gt;/sound2/lvsound2.llb/Sound Output Task ID.ctl"/>
				<Item Name="Recursive File List.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Recursive File List.vi"/>
				<Item Name="List Directory and LLBs.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/List Directory and LLBs.vi"/>
				<Item Name="Sound Output Clear.vi" Type="VI" URL="/&lt;vilib&gt;/sound2/lvsound2.llb/Sound Output Clear.vi"/>
				<Item Name="Sound Output Write.vi" Type="VI" URL="/&lt;vilib&gt;/sound2/lvsound2.llb/Sound Output Write.vi"/>
				<Item Name="Sound Output Write (DBL).vi" Type="VI" URL="/&lt;vilib&gt;/sound2/lvsound2.llb/Sound Output Write (DBL).vi"/>
				<Item Name="Sound Output Write (I16).vi" Type="VI" URL="/&lt;vilib&gt;/sound2/lvsound2.llb/Sound Output Write (I16).vi"/>
				<Item Name="Sound Output Write (I32).vi" Type="VI" URL="/&lt;vilib&gt;/sound2/lvsound2.llb/Sound Output Write (I32).vi"/>
				<Item Name="Sound Output Write (SGL).vi" Type="VI" URL="/&lt;vilib&gt;/sound2/lvsound2.llb/Sound Output Write (SGL).vi"/>
				<Item Name="Sound Output Write (U8).vi" Type="VI" URL="/&lt;vilib&gt;/sound2/lvsound2.llb/Sound Output Write (U8).vi"/>
				<Item Name="Sound Output Write (DBL Single).vi" Type="VI" URL="/&lt;vilib&gt;/sound2/lvsound2.llb/Sound Output Write (DBL Single).vi"/>
				<Item Name="Sound Output Configure.vi" Type="VI" URL="/&lt;vilib&gt;/sound2/lvsound2.llb/Sound Output Configure.vi"/>
				<Item Name="Sampling Mode.ctl" Type="VI" URL="/&lt;vilib&gt;/sound2/lvsound2.llb/Sampling Mode.ctl"/>
				<Item Name="Sound File Info.vi" Type="VI" URL="/&lt;vilib&gt;/sound2/lvsound2.llb/Sound File Info.vi"/>
				<Item Name="Sound File Info (path).vi" Type="VI" URL="/&lt;vilib&gt;/sound2/lvsound2.llb/Sound File Info (path).vi"/>
				<Item Name="Sound File Read.vi" Type="VI" URL="/&lt;vilib&gt;/sound2/lvsound2.llb/Sound File Read.vi"/>
				<Item Name="Sound File Read (I16).vi" Type="VI" URL="/&lt;vilib&gt;/sound2/lvsound2.llb/Sound File Read (I16).vi"/>
				<Item Name="Sound File Read (I32).vi" Type="VI" URL="/&lt;vilib&gt;/sound2/lvsound2.llb/Sound File Read (I32).vi"/>
				<Item Name="Sound File Read (SGL).vi" Type="VI" URL="/&lt;vilib&gt;/sound2/lvsound2.llb/Sound File Read (SGL).vi"/>
				<Item Name="Sound File Read (U8).vi" Type="VI" URL="/&lt;vilib&gt;/sound2/lvsound2.llb/Sound File Read (U8).vi"/>
				<Item Name="IMAQ GetImageInfo" Type="VI" URL="/&lt;vilib&gt;/vision/Basics.llb/IMAQ GetImageInfo"/>
				<Item Name="IMAQ ImageToArray" Type="VI" URL="/&lt;vilib&gt;/vision/Basics.llb/IMAQ ImageToArray"/>
				<Item Name="IMAQ WindSetup" Type="VI" URL="/&lt;vilib&gt;/vision/Display.llb/IMAQ WindSetup"/>
				<Item Name="IMAQ WindDraw" Type="VI" URL="/&lt;vilib&gt;/vision/Display.llb/IMAQ WindDraw"/>
				<Item Name="IMAQ WindClose" Type="VI" URL="/&lt;vilib&gt;/vision/Display.llb/IMAQ WindClose"/>
				<Item Name="IMAQ AVI Get Info" Type="VI" URL="/&lt;vilib&gt;/vision/Avi2.llb/IMAQ AVI Get Info"/>
				<Item Name="IMAQ AVI Open" Type="VI" URL="/&lt;vilib&gt;/vision/Avi2.llb/IMAQ AVI Open"/>
				<Item Name="IMAQ AVI Read Frame" Type="VI" URL="/&lt;vilib&gt;/vision/Avi2.llb/IMAQ AVI Read Frame"/>
				<Item Name="LVDateTimeRec.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVDateTimeRec.ctl"/>
				<Item Name="Get LV Class Path.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Path.vi"/>
				<Item Name="RGB to Color.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/colorconv.llb/RGB to Color.vi"/>
				<Item Name="DAQmx Start Trigger (Analog Edge).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/trigger.llb/DAQmx Start Trigger (Analog Edge).vi"/>
				<Item Name="DAQmx Start Trigger (None).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/trigger.llb/DAQmx Start Trigger (None).vi"/>
				<Item Name="DAQmx Trigger.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/trigger.llb/DAQmx Trigger.vi"/>
				<Item Name="Rendezvous RefNum" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/Rendezvous RefNum"/>
				<Item Name="DAQmx Unflatten Channel String.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/miscellaneous.llb/DAQmx Unflatten Channel String.vi"/>
				<Item Name="Wait at Rendezvous.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/Wait at Rendezvous.vi"/>
				<Item Name="RendezvousDataCluster.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/RendezvousDataCluster.ctl"/>
				<Item Name="Release Waiting Procs.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/Release Waiting Procs.vi"/>
				<Item Name="DAQmx Flatten Channel String.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/miscellaneous.llb/DAQmx Flatten Channel String.vi"/>
				<Item Name="Create Rendezvous.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/Create Rendezvous.vi"/>
				<Item Name="Rendezvous Name &amp; Ref DB Action.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/Rendezvous Name &amp; Ref DB Action.ctl"/>
				<Item Name="Rendezvous Name &amp; Ref DB.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/Rendezvous Name &amp; Ref DB.vi"/>
				<Item Name="Not A Rendezvous.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/Not A Rendezvous.vi"/>
				<Item Name="Create New Rendezvous.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/Create New Rendezvous.vi"/>
				<Item Name="AddNamedRendezvousPrefix.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/AddNamedRendezvousPrefix.vi"/>
				<Item Name="GetNamedRendezvousPrefix.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/GetNamedRendezvousPrefix.vi"/>
				<Item Name="LVStringsAndValuesArrayTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVStringsAndValuesArrayTypeDef.ctl"/>
				<Item Name="LVMinMaxIncTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVMinMaxIncTypeDef.ctl"/>
				<Item Name="SCRT SDV Rtn.vi" Type="VI" URL="/&lt;vilib&gt;/Statechart/Infrastructure/RTStatechart/Dbg/SCRT SDV Rtn.vi"/>
				<Item Name="SCRT Dbg Rtn.vi" Type="VI" URL="/&lt;vilib&gt;/Statechart/Infrastructure/RTStatechart/Dbg/SCRT Dbg Rtn.vi"/>
				<Item Name="DAQmx Create Channel (AI-Voltage-Custom with Excitation).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Voltage-Custom with Excitation).vi"/>
				<Item Name="DAQmx Create Channel (AI-Resistance).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Resistance).vi"/>
				<Item Name="DAQmx Create Channel (AI-Temperature-Thermocouple).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Temperature-Thermocouple).vi"/>
				<Item Name="DAQmx Create Channel (AI-Temperature-RTD).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Temperature-RTD).vi"/>
				<Item Name="DAQmx Create Channel (AI-Temperature-Thermistor-Iex).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Temperature-Thermistor-Iex).vi"/>
				<Item Name="DAQmx Create Channel (AI-Temperature-Thermistor-Vex).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Temperature-Thermistor-Vex).vi"/>
				<Item Name="DAQmx Create Channel (AO-FuncGen).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AO-FuncGen).vi"/>
				<Item Name="DAQmx Create Channel (CI-Frequency).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Frequency).vi"/>
				<Item Name="DAQmx Create Channel (CI-Period).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Period).vi"/>
				<Item Name="DAQmx Create Channel (CI-Count Edges).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Count Edges).vi"/>
				<Item Name="DAQmx Create Channel (CI-Pulse Width).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Pulse Width).vi"/>
				<Item Name="DAQmx Create Channel (CI-Semi Period).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Semi Period).vi"/>
				<Item Name="DAQmx Create Channel (AI-Current-Basic).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Current-Basic).vi"/>
				<Item Name="DAQmx Create Channel (AI-Strain-Strain Gage).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Strain-Strain Gage).vi"/>
				<Item Name="DAQmx Create Channel (AI-Temperature-Built-in Sensor).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Temperature-Built-in Sensor).vi"/>
				<Item Name="DAQmx Create Channel (AI-Frequency-Voltage).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Frequency-Voltage).vi"/>
				<Item Name="DAQmx Create Channel (CO-Pulse Generation-Ticks).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CO-Pulse Generation-Ticks).vi"/>
				<Item Name="DAQmx Create Channel (AI-Position-LVDT).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Position-LVDT).vi"/>
				<Item Name="DAQmx Create Channel (AI-Position-RVDT).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Position-RVDT).vi"/>
				<Item Name="DAQmx Create Channel (CI-Two Edge Separation).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Two Edge Separation).vi"/>
				<Item Name="DAQmx Create Channel (AI-Acceleration-Accelerometer).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Acceleration-Accelerometer).vi"/>
				<Item Name="DAQmx Create Channel (CI-Position-Angular Encoder).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Position-Angular Encoder).vi"/>
				<Item Name="DAQmx Create Channel (CI-Position-Linear Encoder).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Position-Linear Encoder).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Acceleration-Accelerometer).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Acceleration-Accelerometer).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Current-Basic).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Current-Basic).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Position-LVDT).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Position-LVDT).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Position-RVDT).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Position-RVDT).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Resistance).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Resistance).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Strain-Strain Gage).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Strain-Strain Gage).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Temperature-RTD).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Temperature-RTD).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Temperature-Thermistor-Iex).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Temperature-Thermistor-Iex).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Temperature-Thermistor-Vex).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Temperature-Thermistor-Vex).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Voltage-Basic).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Voltage-Basic).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Voltage-Custom with Excitation).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Voltage-Custom with Excitation).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Temperature-Thermocouple).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Temperature-Thermocouple).vi"/>
				<Item Name="DAQmx Create Channel (AI-Sound Pressure-Microphone).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Sound Pressure-Microphone).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Sound Pressure-Microphone).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Sound Pressure-Microphone).vi"/>
				<Item Name="DAQmx Create Channel (CI-GPS Timestamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-GPS Timestamp).vi"/>
				<Item Name="DAQmx Create Channel (AO-Current-Basic).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AO-Current-Basic).vi"/>
				<Item Name="DAQmx Create Channel (AI-Voltage-RMS).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Voltage-RMS).vi"/>
				<Item Name="DAQmx Create Channel (AI-Current-RMS).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Current-RMS).vi"/>
				<Item Name="DAQmx Create Channel (AI-Position-EddyCurrentProxProbe).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Position-EddyCurrentProxProbe).vi"/>
				<Item Name="DAQmx Create Channel (CI-Pulse Freq).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Pulse Freq).vi"/>
				<Item Name="DAQmx Create Channel (CI-Pulse Time).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Pulse Time).vi"/>
				<Item Name="DAQmx Create Channel (CI-Pulse Ticks).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Pulse Ticks).vi"/>
				<Item Name="DAQmx Create Channel (AI-Bridge).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Bridge).vi"/>
				<Item Name="DAQmx Create Channel (AI-Force-Bridge-Polynomial).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Force-Bridge-Polynomial).vi"/>
				<Item Name="DAQmx Create Channel (AI-Force-Bridge-Table).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Force-Bridge-Table).vi"/>
				<Item Name="DAQmx Create Channel (AI-Force-Bridge-Two-Point-Linear).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Force-Bridge-Two-Point-Linear).vi"/>
				<Item Name="DAQmx Create Channel (AI-Pressure-Bridge-Two-Point-Linear).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Pressure-Bridge-Two-Point-Linear).vi"/>
				<Item Name="DAQmx Create Channel (AI-Pressure-Bridge-Table).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Pressure-Bridge-Table).vi"/>
				<Item Name="DAQmx Create Channel (AI-Pressure-Bridge-Polynomial).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Pressure-Bridge-Polynomial).vi"/>
				<Item Name="DAQmx Create Channel (AI-Torque-Bridge-Two-Point-Linear).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Torque-Bridge-Two-Point-Linear).vi"/>
				<Item Name="DAQmx Create Channel (AI-Torque-Bridge-Table).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Torque-Bridge-Table).vi"/>
				<Item Name="DAQmx Create Channel (AI-Torque-Bridge-Polynomial).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Torque-Bridge-Polynomial).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Force-Bridge).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Force-Bridge).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Pressure-Bridge).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Pressure-Bridge).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Torque-Bridge).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Torque-Bridge).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Force-IEPE Sensor).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Force-IEPE Sensor).vi"/>
				<Item Name="DAQmx Create Channel (AI-Force-IEPE Sensor).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Force-IEPE Sensor).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Bridge).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Bridge).vi"/>
				<Item Name="DAQmx Timing (Handshaking).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/timing.llb/DAQmx Timing (Handshaking).vi"/>
				<Item Name="DAQmx Timing (Use Waveform).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/timing.llb/DAQmx Timing (Use Waveform).vi"/>
				<Item Name="DAQmx Timing (Change Detection).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/timing.llb/DAQmx Timing (Change Detection).vi"/>
				<Item Name="DAQmx Timing (Burst Import Clock).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/timing.llb/DAQmx Timing (Burst Import Clock).vi"/>
				<Item Name="DAQmx Timing (Burst Export Clock).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/timing.llb/DAQmx Timing (Burst Export Clock).vi"/>
				<Item Name="DAQmx Timing (Pipelined Sample Clock).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/timing.llb/DAQmx Timing (Pipelined Sample Clock).vi"/>
				<Item Name="IMAQ Image.ctl" Type="VI" URL="/&lt;vilib&gt;/vision/Image Controls.llb/IMAQ Image.ctl"/>
				<Item Name="NI_Vision_Development_Module.lvlib" Type="Library" URL="/&lt;vilib&gt;/vision/NI_Vision_Development_Module.lvlib"/>
				<Item Name="Image Unit" Type="VI" URL="/&lt;vilib&gt;/vision/Image Controls.llb/Image Unit"/>
				<Item Name="Color (U64)" Type="VI" URL="/&lt;vilib&gt;/vision/Image Controls.llb/Color (U64)"/>
				<Item Name="AviRefnum.ctl" Type="VI" URL="/&lt;vilib&gt;/vision/Avi1.llb/AviRefnum.ctl"/>
				<Item Name="DAQmx Write (Analog 1D DBL NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog 1D DBL NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Analog 1D Wfm NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog 1D Wfm NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Analog DBL 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog DBL 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Analog Wfm 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog Wfm 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Analog Wfm 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog Wfm 1Chan NSamp).vi"/>
				<Item Name="DAQmx Write (Digital 2D U32 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 2D U32 NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Digital 1D U32 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D U32 1Chan NSamp).vi"/>
				<Item Name="DAQmx Write (Digital 1D U8 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D U8 1Chan NSamp).vi"/>
				<Item Name="DAQmx Write (Digital Bool 1Line 1Point).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital Bool 1Line 1Point).vi"/>
				<Item Name="DAQmx Write (Digital Wfm 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital Wfm 1Chan NSamp).vi"/>
				<Item Name="DWDT Uncompress Digital.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DWDTOps.llb/DWDT Uncompress Digital.vi"/>
				<Item Name="DTbl Uncompress Digital.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DTblOps.llb/DTbl Uncompress Digital.vi"/>
				<Item Name="DTbl Digital Size.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DTblOps.llb/DTbl Digital Size.vi"/>
				<Item Name="DAQmx Write (Raw 1D I16).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Raw 1D I16).vi"/>
				<Item Name="DAQmx Write (Raw 1D I32).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Raw 1D I32).vi"/>
				<Item Name="DAQmx Write (Raw 1D I8).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Raw 1D I8).vi"/>
				<Item Name="DAQmx Write (Raw 1D U16).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Raw 1D U16).vi"/>
				<Item Name="DAQmx Write (Raw 1D U32).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Raw 1D U32).vi"/>
				<Item Name="DAQmx Write (Raw 1D U8).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Raw 1D U8).vi"/>
				<Item Name="DAQmx Write (Digital 1D Wfm NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D Wfm NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital Wfm 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital Wfm 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Analog 1D Wfm NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog 1D Wfm NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Digital 1D Wfm NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D Wfm NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Digital U8 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital U8 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital U32 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital U32 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital 1D U32 NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D U32 NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital 1D U8 NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D U8 NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital 2D Bool NChan 1Samp NLine).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 2D Bool NChan 1Samp NLine).vi"/>
				<Item Name="DAQmx Write (Digital 1D Bool NChan 1Samp 1Line).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D Bool NChan 1Samp 1Line).vi"/>
				<Item Name="DAQmx Write (Analog 2D I16 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog 2D I16 NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Analog 2D U16 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog 2D U16 NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Counter Frequency 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter Frequency 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Counter Ticks 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter Ticks 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Counter Time 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter Time 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Counter 1D Frequency NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter 1D Frequency NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Counter 1D Time NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter 1D Time NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Counter 1DTicks NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter 1DTicks NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Analog 2D I32 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog 2D I32 NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Digital U16 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital U16 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital 1D U16 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D U16 1Chan NSamp).vi"/>
				<Item Name="DAQmx Write (Digital 1D U16 NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D U16 NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital 2D U16 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 2D U16 NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Counter 1D Frequency 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter 1D Frequency 1Chan NSamp).vi"/>
				<Item Name="DAQmx Write (Counter 1D Ticks 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter 1D Ticks 1Chan NSamp).vi"/>
				<Item Name="DAQmx Write (Counter 1D Time 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter 1D Time 1Chan NSamp).vi"/>
				<Item Name="DAQmx Start Trigger (Digital Edge).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/trigger.llb/DAQmx Start Trigger (Digital Edge).vi"/>
				<Item Name="DAQmx Reference Trigger (Analog Edge).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/trigger.llb/DAQmx Reference Trigger (Analog Edge).vi"/>
				<Item Name="DAQmx Advance Trigger (Digital Edge).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/trigger.llb/DAQmx Advance Trigger (Digital Edge).vi"/>
				<Item Name="DAQmx Reference Trigger (None).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/trigger.llb/DAQmx Reference Trigger (None).vi"/>
				<Item Name="DAQmx Reference Trigger (Digital Edge).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/trigger.llb/DAQmx Reference Trigger (Digital Edge).vi"/>
				<Item Name="DAQmx Start Trigger (Analog Window).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/trigger.llb/DAQmx Start Trigger (Analog Window).vi"/>
				<Item Name="DAQmx Reference Trigger (Analog Window).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/trigger.llb/DAQmx Reference Trigger (Analog Window).vi"/>
				<Item Name="DAQmx Advance Trigger (None).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/trigger.llb/DAQmx Advance Trigger (None).vi"/>
				<Item Name="DAQmx Reference Trigger (Digital Pattern).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/trigger.llb/DAQmx Reference Trigger (Digital Pattern).vi"/>
				<Item Name="DAQmx Start Trigger (Digital Pattern).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/trigger.llb/DAQmx Start Trigger (Digital Pattern).vi"/>
				<Item Name="DAQmx Read (Analog 1D Wfm NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 1D Wfm NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Analog 1D DBL 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 1D DBL 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Analog 1D DBL NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 1D DBL NChan 1Samp).vi"/>
				<Item Name="DAQmx Read (Analog 1D Wfm NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 1D Wfm NChan 1Samp).vi"/>
				<Item Name="DAQmx Read (Analog DBL 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog DBL 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Analog Wfm 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog Wfm 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Analog Wfm 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog Wfm 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Digital 1D Bool 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D Bool 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital 1D U32 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D U32 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Digital 1D U8 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D U8 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Digital 1D Wfm NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D Wfm NChan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital 2D U32 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 2D U32 NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Digital Bool 1Line 1Point).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital Bool 1Line 1Point).vi"/>
				<Item Name="DAQmx Read (Digital Wfm 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital Wfm 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Raw 1D I16).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Raw 1D I16).vi"/>
				<Item Name="DAQmx Read (Raw 1D I32).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Raw 1D I32).vi"/>
				<Item Name="DAQmx Read (Raw 1D I8).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Raw 1D I8).vi"/>
				<Item Name="DAQmx Read (Raw 1D U16).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Raw 1D U16).vi"/>
				<Item Name="DAQmx Read (Raw 1D U32).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Raw 1D U32).vi"/>
				<Item Name="DAQmx Read (Raw 1D U8).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Raw 1D U8).vi"/>
				<Item Name="DAQmx Read (Digital 1D Wfm NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D Wfm NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Digital Wfm 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital Wfm 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Counter 1D DBL 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 1D DBL 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Counter DBL 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter DBL 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Counter U32 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter U32 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Counter 1D U32 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 1D U32 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Digital 1D U8 NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D U8 NChan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital 1D U32 NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D U32 NChan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital U8 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital U8 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital U32 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital U32 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital 1D Bool NChan 1Samp 1Line).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D Bool NChan 1Samp 1Line).vi"/>
				<Item Name="DAQmx Read (Digital 2D Bool NChan 1Samp NLine).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 2D Bool NChan 1Samp NLine).vi"/>
				<Item Name="DAQmx Read (Analog 2D U16 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 2D U16 NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Analog 2D I16 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 2D I16 NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Analog 2D I32 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 2D I32 NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Analog 2D U32 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 2D U32 NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Digital U16 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital U16 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital 1D U16 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D U16 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Digital 1D U16 NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D U16 NChan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital 2D U16 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 2D U16 NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Counter 1D Pulse Freq 1 Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 1D Pulse Freq 1 Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Counter 1D Pulse Ticks 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 1D Pulse Ticks 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Counter 1D Pulse Time 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 1D Pulse Time 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Counter Pulse Freq 1 Chan 1 Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter Pulse Freq 1 Chan 1 Samp).vi"/>
				<Item Name="DAQmx Read (Counter Pulse Ticks 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter Pulse Ticks 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Counter Pulse Time 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter Pulse Time 1Chan 1Samp).vi"/>
				<Item Name="NI_SC_LVSCCommonFiles.lvlib" Type="Library" URL="/&lt;vilib&gt;/Statechart/Common/NI_SC_LVSCCommonFiles.lvlib"/>
				<Item Name="NI-845x I2C Create Configuration Reference.vi" Type="VI" URL="/&lt;vilib&gt;/ni845x/ni845x.llb/NI-845x I2C Create Configuration Reference.vi"/>
				<Item Name="NI-845x I2C Write.vi" Type="VI" URL="/&lt;vilib&gt;/ni845x/ni845x.llb/NI-845x I2C Write.vi"/>
				<Item Name="NI-845x I2C Read.vi" Type="VI" URL="/&lt;vilib&gt;/ni845x/ni845x.llb/NI-845x I2C Read.vi"/>
				<Item Name="NI-845x Close Reference.vi" Type="VI" URL="/&lt;vilib&gt;/ni845x/ni845x.llb/NI-845x Close Reference.vi"/>
				<Item Name="NI-845x Close Device Reference.vi" Type="VI" URL="/&lt;vilib&gt;/ni845x/ni845x.llb/NI-845x Close Device Reference.vi"/>
				<Item Name="NI-845x I2C Close Configuration Reference.vi" Type="VI" URL="/&lt;vilib&gt;/ni845x/ni845x.llb/NI-845x I2C Close Configuration Reference.vi"/>
				<Item Name="errorList.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/inputDevices.llb/errorList.vi"/>
				<Item Name="mouseAcquire.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/inputDevices.llb/mouseAcquire.vi"/>
				<Item Name="keyboardAcquire.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/inputDevices.llb/keyboardAcquire.vi"/>
				<Item Name="joystickAcquire.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/inputDevices.llb/joystickAcquire.vi"/>
				<Item Name="Acquire Input Data.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/inputDevices.llb/Acquire Input Data.vi"/>
				<Item Name="Intialize Keyboard.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/inputDevices.llb/Intialize Keyboard.vi"/>
				<Item Name="closeMouse.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/inputDevices.llb/closeMouse.vi"/>
				<Item Name="closeKeyboard.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/inputDevices.llb/closeKeyboard.vi"/>
				<Item Name="closeJoystick.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/inputDevices.llb/closeJoystick.vi"/>
				<Item Name="Close Input Device.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/inputDevices.llb/Close Input Device.vi"/>
				<Item Name="NI_PackedLibraryUtility.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/LVLibp/NI_PackedLibraryUtility.lvlib"/>
				<Item Name="DAQmx Create Channel (AI-Velocity-IEPE Sensor).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Velocity-IEPE Sensor).vi"/>
				<Item Name="DAQmx Create Channel (AI-Strain-Rosette Strain Gage).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Strain-Rosette Strain Gage).vi"/>
				<Item Name="ErrorDescriptions.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/inputDevices.llb/ErrorDescriptions.vi"/>
				<Item Name="ni845xControl.ctl" Type="VI" URL="/&lt;vilib&gt;/ni845x/ni845x.llb/ni845xControl.ctl"/>
				<Item Name="Get LV Class Name.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Name.vi"/>
				<Item Name="LVStringsAndValuesArrayTypeDef_U16.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVStringsAndValuesArrayTypeDef_U16.ctl"/>
				<Item Name="subFile Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/express/express input/FileDialogBlock.llb/subFile Dialog.vi"/>
				<Item Name="ex_CorrectErrorChain.vi" Type="VI" URL="/&lt;vilib&gt;/express/express shared/ex_CorrectErrorChain.vi"/>
				<Item Name="General Error Handler Core CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler Core CORE.vi"/>
				<Item Name="LVRectTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRectTypeDef.ctl"/>
				<Item Name="DAQmx Create Channel (CI-Duty Cycle).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Duty Cycle).vi"/>
				<Item Name="DAQmx Read (Counter 1D DBL NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 1D DBL NChan 1Samp).vi"/>
				<Item Name="DAQmx Read (Counter 1D U32 NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 1D U32 NChan 1Samp).vi"/>
				<Item Name="DAQmx Read (Counter 2D DBL NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 2D DBL NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Counter 2D U32 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 2D U32 NChan NSamp).vi"/>
				<Item Name="High Resolution Relative Seconds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/High Resolution Relative Seconds.vi"/>
				<Item Name="DAQmx Create Channel (CI-Velocity-Angular).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Velocity-Angular).vi"/>
				<Item Name="DAQmx Create Channel (CI-Velocity-Linear).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Velocity-Linear).vi"/>
				<Item Name="DAQmx Create Channel (AI-Acceleration-4 Wire DC Voltage).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Acceleration-4 Wire DC Voltage).vi"/>
				<Item Name="DAQmx Create Channel (AI-Acceleration-Charge).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Acceleration-Charge).vi"/>
				<Item Name="DAQmx Create Channel (AI-Charge).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Charge).vi"/>
				<Item Name="DAQmx Start Trigger (Time).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/trigger.llb/DAQmx Start Trigger (Time).vi"/>
				<Item Name="DAQmx Start Trigger (Analog Multi Edge).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/trigger.llb/DAQmx Start Trigger (Analog Multi Edge).vi"/>
				<Item Name="DAQmx Reference Trigger (Analog Multi Edge).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/trigger.llb/DAQmx Reference Trigger (Analog Multi Edge).vi"/>
				<Item Name="DAQmx Read (Analog Wfm 1Chan NSamp Duration).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog Wfm 1Chan NSamp Duration).vi"/>
				<Item Name="DAQmx Read (Analog 1D Wfm NChan NSamp Duration).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 1D Wfm NChan NSamp Duration).vi"/>
				<Item Name="DAQmx Read (Digital Wfm 1Chan NSamp Duration).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital Wfm 1Chan NSamp Duration).vi"/>
				<Item Name="DAQmx Read (Digital 1D Wfm NChan NSamp Duration).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D Wfm NChan NSamp Duration).vi"/>
				<Item Name="NI_Vision_Acquisition_Software.lvlib" Type="Library" URL="/&lt;vilib&gt;/vision/driver/NI_Vision_Acquisition_Software.lvlib"/>
				<Item Name="ni845xI2cConfiguration.ctl" Type="VI" URL="/&lt;vilib&gt;/ni845x/ni845x.llb/ni845xI2cConfiguration.ctl"/>
				<Item Name="NI-845x Fill in Error Info.vi" Type="VI" URL="/&lt;vilib&gt;/ni845x/ni845x.llb/NI-845x Fill in Error Info.vi"/>
				<Item Name="NI-845x SPI Close Configuration Reference.vi" Type="VI" URL="/&lt;vilib&gt;/ni845x/ni845x.llb/NI-845x SPI Close Configuration Reference.vi"/>
				<Item Name="ni845xSpiConfiguration.ctl" Type="VI" URL="/&lt;vilib&gt;/ni845x/ni845x.llb/ni845xSpiConfiguration.ctl"/>
				<Item Name="NI-845x SPI Close Script Reference.vi" Type="VI" URL="/&lt;vilib&gt;/ni845x/ni845x.llb/NI-845x SPI Close Script Reference.vi"/>
				<Item Name="ni845xSpiScriptControl.ctl" Type="VI" URL="/&lt;vilib&gt;/ni845x/ni845x.llb/ni845xSpiScriptControl.ctl"/>
				<Item Name="NI-845x I2C Close Script Reference.vi" Type="VI" URL="/&lt;vilib&gt;/ni845x/ni845x.llb/NI-845x I2C Close Script Reference.vi"/>
				<Item Name="ni845xI2cScriptControl.ctl" Type="VI" URL="/&lt;vilib&gt;/ni845x/ni845x.llb/ni845xI2cScriptControl.ctl"/>
				<Item Name="NI-845x SPI Stream Close Configuration Reference.vi" Type="VI" URL="/&lt;vilib&gt;/ni845x/ni845x.llb/NI-845x SPI Stream Close Configuration Reference.vi"/>
				<Item Name="ni845xSpiStreamConfiguration.ctl" Type="VI" URL="/&lt;vilib&gt;/ni845x/ni845x.llb/ni845xSpiStreamConfiguration.ctl"/>
				<Item Name="NI-845x I2C Slave Close Configuration Reference.vi" Type="VI" URL="/&lt;vilib&gt;/ni845x/ni845x.llb/NI-845x I2C Slave Close Configuration Reference.vi"/>
				<Item Name="ni845xI2cSlaveConfiguration.ctl" Type="VI" URL="/&lt;vilib&gt;/ni845x/ni845x.llb/ni845xI2cSlaveConfiguration.ctl"/>
				<Item Name="ClearError.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tdmsutil.llb/ClearError.vi"/>
			</Item>
			<Item Name="nilvaiu.dll" Type="Document" URL="nilvaiu.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="System" Type="VI" URL="System">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="kernel32.dll" Type="Document" URL="kernel32.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="System.Windows.Forms" Type="Document" URL="System.Windows.Forms">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="nivissvc.dll" Type="Document" URL="nivissvc.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="nivision.dll" Type="Document" URL="nivision.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="lvsound2.dll" Type="Document" URL="/&lt;resource&gt;/lvsound2.dll"/>
			<Item Name="lvanlys.dll" Type="Document" URL="/&lt;resource&gt;/lvanlys.dll"/>
			<Item Name="lvinput.dll" Type="Document" URL="/&lt;resource&gt;/lvinput.dll"/>
			<Item Name="niimaqdx.dll" Type="Document" URL="niimaqdx.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="ni845x.dll" Type="Document" URL="ni845x.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
		</Item>
		<Item Name="Build Specifications" Type="Build">
			<Item Name="recOOrder" Type="EXE">
				<Property Name="App_INI_aliasGUID" Type="Str">{BBF33DB7-7AD4-4710-A5A9-F7FEF059CF17}</Property>
				<Property Name="App_INI_GUID" Type="Str">{4BA30A1C-7439-4A2B-AB2C-EC712D72AB1B}</Property>
				<Property Name="App_INI_itemID" Type="Ref">/My Computer/recorder_settings.ini</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{72639938-3A65-4FC1-8987-895A94904231}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">recOOrder</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../Build</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToProject</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{4415BDEA-1EA2-47FD-AB9A-A19D204BD0A2}</Property>
				<Property Name="Bld_version.build" Type="Int">94</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Bld_version.patch" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">recOOrder.exe</Property>
				<Property Name="Destination[0].path" Type="Path">../Build/NI_AB_PROJECTNAME.exe</Property>
				<Property Name="Destination[0].path.type" Type="Str">relativeToProject</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../Build/data</Property>
				<Property Name="Destination[1].path.type" Type="Str">relativeToProject</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Exe_iconItemID" Type="Ref">/My Computer/finch16x16.ico</Property>
				<Property Name="Source[0].itemID" Type="Str">{BFCF5D0D-76CE-48BE-9970-C5C887A4B082}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/recoorder.vi</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="Source[10].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[10].itemID" Type="Ref">/My Computer/plugins/UDP webcam get send/UDP get send.vi</Property>
				<Property Name="Source[10].type" Type="Str">VI</Property>
				<Property Name="Source[11].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[11].itemID" Type="Ref">/My Computer/plugins/soundcard/soundcard.vi</Property>
				<Property Name="Source[11].type" Type="Str">VI</Property>
				<Property Name="Source[12].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[12].itemID" Type="Ref">/My Computer/plugins/labnotes/labnotes.vi</Property>
				<Property Name="Source[12].type" Type="Str">VI</Property>
				<Property Name="Source[13].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[13].itemID" Type="Ref">/My Computer/fgs/boolean/boolean_fg.vit</Property>
				<Property Name="Source[13].type" Type="Str">VI</Property>
				<Property Name="Source[14].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[14].itemID" Type="Ref">/My Computer/fgs/string fg/string_fg.vit</Property>
				<Property Name="Source[14].type" Type="Str">VI</Property>
				<Property Name="Source[15].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[15].itemID" Type="Ref">/My Computer/plugins/plot notifications/plot notifications.vi</Property>
				<Property Name="Source[15].type" Type="Str">VI</Property>
				<Property Name="Source[16].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[16].itemID" Type="Ref">/My Computer/help/help.vi</Property>
				<Property Name="Source[16].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[16].type" Type="Str">VI</Property>
				<Property Name="Source[17].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[17].itemID" Type="Ref">/My Computer/plugins/intracellular/intracellular song playback.vi</Property>
				<Property Name="Source[17].type" Type="Str">VI</Property>
				<Property Name="Source[18].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[18].itemID" Type="Ref">/My Computer/plugins/UDP webcam/UDP webcam.vit</Property>
				<Property Name="Source[18].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[18].type" Type="Str">VI</Property>
				<Property Name="Source[19].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[19].itemID" Type="Ref">/My Computer/plugins/collision test.vi</Property>
				<Property Name="Source[19].type" Type="Str">VI</Property>
				<Property Name="Source[2].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[2].itemID" Type="Ref">/My Computer/plugins/mdcontrol/microdrive controller new.vi</Property>
				<Property Name="Source[2].type" Type="Str">VI</Property>
				<Property Name="Source[20].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[20].itemID" Type="Ref">/My Computer/plugins/mdcontrol/microdrive controller.vi</Property>
				<Property Name="Source[20].type" Type="Str">VI</Property>
				<Property Name="Source[21].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[21].itemID" Type="Ref">/My Computer/plugins/labnotes/labnotes-tags.txt</Property>
				<Property Name="Source[21].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[22].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[22].itemID" Type="Ref">/My Computer/plugins/write digital channel.vi</Property>
				<Property Name="Source[22].type" Type="Str">VI</Property>
				<Property Name="Source[23].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[23].itemID" Type="Ref">/My Computer/controls/investigators.txt</Property>
				<Property Name="Source[23].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[24].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[24].itemID" Type="Ref">/My Computer/controls/setups.txt</Property>
				<Property Name="Source[24].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[25].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[25].itemID" Type="Ref">/My Computer/plugins/UDP michi/UDP webcam michi.vi</Property>
				<Property Name="Source[25].type" Type="Str">VI</Property>
				<Property Name="Source[26].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[26].itemID" Type="Ref">/My Computer/plugins/air puffs/air puffs multi setup.vit</Property>
				<Property Name="Source[26].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[26].type" Type="Str">VI</Property>
				<Property Name="Source[27].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[27].itemID" Type="Ref">/My Computer/plugins/TwoPhotonMic/voltage2laserpower.vi</Property>
				<Property Name="Source[27].type" Type="Str">VI</Property>
				<Property Name="Source[28].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[28].itemID" Type="Ref">/My Computer/plugins/air puffs/air puffs multi setup multiWAV.vit</Property>
				<Property Name="Source[28].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[28].type" Type="Str">VI</Property>
				<Property Name="Source[29].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[29].itemID" Type="Ref">/My Computer/plugins/mdcontrol2/microdrive_controller_2.vi</Property>
				<Property Name="Source[29].type" Type="Str">VI</Property>
				<Property Name="Source[3].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[3].itemID" Type="Ref">/My Computer/plugins/effector interface.vi</Property>
				<Property Name="Source[3].type" Type="Str">VI</Property>
				<Property Name="Source[30].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[30].itemID" Type="Ref">/My Computer/plugins/playback/song playback.vit</Property>
				<Property Name="Source[30].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[30].type" Type="Str">VI</Property>
				<Property Name="Source[31].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[31].itemID" Type="Ref">/My Computer/plugins/air puffs/air puffs.vit</Property>
				<Property Name="Source[31].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[31].type" Type="Str">VI</Property>
				<Property Name="Source[32].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[32].itemID" Type="Ref">/My Computer/plugins/air puffs/air puffs new.vit</Property>
				<Property Name="Source[32].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[32].type" Type="Str">VI</Property>
				<Property Name="Source[4].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[4].itemID" Type="Ref">/My Computer/fgs/1darray/1d_array_fg.vit</Property>
				<Property Name="Source[4].type" Type="Str">VI</Property>
				<Property Name="Source[5].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[5].itemID" Type="Ref">/My Computer/plugins/oscilloscope/oscilloscope.vi</Property>
				<Property Name="Source[5].type" Type="Str">VI</Property>
				<Property Name="Source[6].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[6].itemID" Type="Ref">/My Computer/plugins/acute/Gen Cont or Single Current Pulse.vi</Property>
				<Property Name="Source[6].type" Type="Str">VI</Property>
				<Property Name="Source[7].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[7].itemID" Type="Ref">/My Computer/plugins/beta quality/webcam/webcam.vi</Property>
				<Property Name="Source[7].type" Type="Str">VI</Property>
				<Property Name="Source[8].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[8].itemID" Type="Ref">/My Computer/plugins/trigger logger/trigger logger.vi</Property>
				<Property Name="Source[8].type" Type="Str">VI</Property>
				<Property Name="Source[9].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[9].itemID" Type="Ref">/My Computer/fgs/double fg/scalar_fg.vit</Property>
				<Property Name="Source[9].type" Type="Str">VI</Property>
				<Property Name="SourceCount" Type="Int">33</Property>
				<Property Name="TgtF_companyName" Type="Str">ETHZ</Property>
				<Property Name="TgtF_enableDebugging" Type="Bool">true</Property>
				<Property Name="TgtF_fileDescription" Type="Str">recOOrder</Property>
				<Property Name="TgtF_internalName" Type="Str">recOOrder</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright  2022 ETHZ</Property>
				<Property Name="TgtF_productName" Type="Str">recOOrder</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{F0483BAA-4A25-4BCE-8163-E06BA7A5F83E}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">recOOrder.exe</Property>
			</Item>
		</Item>
	</Item>
</Project>
